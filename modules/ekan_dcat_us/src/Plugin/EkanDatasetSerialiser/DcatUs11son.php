<?php

namespace Drupal\ekan_dcat_us\Plugin\EkanDatasetSerialiser;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Url;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserBase;
use Drupal\file\FileInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\link\Plugin\Field\FieldType\LinkItem;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * A serialiser to support POD v1.1 XML data.
 *
 * @EkanDatasetSerialiser(
 *     id = "dcat_us_1_1_json",
 *     label = @Translation("DCAT POD 1.1 JSON"),
 *     format = "json"
 *   )
 * /
 */
class DcatUs11son extends EkanDatasetSerialiserBase {

  /**
   * {@inheritDoc}
   */
  public function getUrlForDataset(EkanDatasetEntity $dataset): ?Url {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getMimeType(): string {
    return 'application/json';
  }

  /**
   * {@inheritDoc}
   */
  public function serialiseBuild(array $build): string {
    $format = $this->getFormat();
    $encoder = new JsonEncoder();
    $normalizer = new GetSetMethodNormalizer();
    $serializer = new Serializer([$normalizer], [$encoder]);
    return $serializer->serialize($build, $format);
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $dataset_ids = []): array {
    return [
      '@context' => 'https://project-open-data.cio.gov/v1.1/schema/catalog.jsonld',
      '@id' => $this->getUrlForFullCatalog()->setAbsolute()->toString(TRUE)->getGeneratedUrl(),
      '@type' => 'dcat:Catalog',
      'conformsTo' => 'https://project-open-data.cio.gov/v1.1/schema',
      'describedBy' => 'https://project-open-data.cio.gov/v1.1/schema/catalog.json',
      'dataset' => $this->buildDatasets($dataset_ids),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildDataset(EkanDatasetEntity $dataset): array {

    $keywords = [];
    $tags_field = $dataset->get('tags');
    assert($tags_field instanceof EntityReferenceFieldItemListInterface);
    foreach ($tags_field->referencedEntities() as $term) {
      assert($term instanceof TermInterface);
      $keywords[] = $term->label();
    }

    $themes = [];
    foreach ($dataset->get('pod_theme')->getValue() as $item) {
      $themes[] = $item['value'];
    }

    if (empty($themes)) {
      $topic_field = $dataset->get('topic');
      assert($topic_field instanceof EntityReferenceFieldItemListInterface);
      foreach ($topic_field->referencedEntities() as $term) {
        assert($term instanceof TermInterface);
        $themes[] = $term->label();
      }
    }
    // Get path alias first as dataset->url() not returning path_alias on
    // programmatic entity update process.
    $alias_manager = \Drupal::service('path_alias.manager');
    $alias = $alias_manager->getAliasByPath('/dataset/' . $dataset->id());
    // If no path alias then get the absolute link to the url.
    if (!$alias) {
      $landing_page = $dataset->toUrl('uuid_redirect')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    }
    else {
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $landing_page = $host . $alias;
    }
    if (!$dataset->get('landing_page')->isEmpty()) {
      $landing_page = $dataset->get('landing_page')->getString();
    }

    $contact_name = $dataset->get('uid')->entity ? $dataset->get('uid')->entity->label() : '';
    if (!$dataset->get('contact_name')->isEmpty()) {
      $contact_name = $dataset->get('contact_name')->getString();
    }

    $references = [];
    foreach ($dataset->get('related_content') as $item) {
      assert($item instanceof LinkItem);
      $references[] = $item->getUrl()->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    }

    // @todo support for mult-value publisher.
    $publisher = $dataset->get('publisher')->entity;
    if ($publisher instanceof GroupInterface) {
      $publisher = [
        '@type' => "org:Organization",
        'name' => $publisher->label(),
      ];
    }
    else {
      $publisher = NULL;
    }

    $modified = date('c', $dataset->get('changed')->value);
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('ekan_harvest') && !$dataset->get('harvest_source_modified')->isEmpty()) {
      $modified = date('c', $dataset->get('harvest_source_modified')->value);
    }

    $temporal = NULL;
    $start_date = $dataset->get('temporal_coverage')->start_date;
    $end_date = $dataset->get('temporal_coverage')->end_date;
    if ($start_date && $end_date) {
      $temporal = $start_date->format('c') . '/' . $end_date->format('c');
    }

    $data = [
      '@type' => 'dcat:Dataset',
      'accessLevel' => $dataset->get('public_access_level')->getString(),
      'rights' => $dataset->get('rights')->value,
      'accrualPeriodicity' => $dataset->get('frequency')->getString(),
      // 'bureauCode' => '',
      'contactPoint' => [
        'fn' => $contact_name,
        'hasEmail' => $dataset->get('contact_email')->getString(),
      ],
      'describedBy' => $dataset->get('data_dictionary')->value,
      'describedByType' => $dataset->get('data_dictionary_type')->getString(),
      'conformsTo' => $dataset->get('conforms_to')->getString(),
      // 'dataQuality' => '',
      'description' => $dataset->get('body')->value,
      'distribution' => $this->buildDistributions($dataset),
      'identifier' => $dataset->get('uuid')->value,
      'issued' => date('c', $dataset->get('created')->value),
      'keyword' => $keywords,
      'landingPage' => $landing_page,
      'language' => $dataset->get('language')->getString(),
      'license' => $dataset->get('license')->getString(),
      'modified' => $modified,
      // 'primaryITInvestmentUII' => '',
      // 'programCode' => '',
      'publisher' => $publisher,
      'references' => $references,
      'spatial' => $dataset->get('spatial')->value,
      // 'systemOfRecords' => '',
      'temporal' => $temporal,
      'isPartOf' => $dataset->get('is_part_of')->getString(),
      'theme' => $themes,
      'title' => $dataset->label(),
    ];

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function buildResource(EkanResourceEntity $resource): array {
    $upload = $resource->get('upload')->entity;
    $download_url = NULL;
    $media_type = NULL;
    $api_link = NULL;
    $format = NULL;
    $resource_type = NULL;

    if ($resource->get('resource_format')->entity) {
      $format = $resource->get('resource_format')->entity->label();
    }

    if ($resource->get('resource_type')->entity) {
      $resource_type = $resource->get('resource_type')->entity->label();
    }

    if ($upload instanceof FileInterface) {
      $download_url = $upload->createFileUrl(FALSE);
      $media_type = $upload->getMimeType();
    }

    $remote_file = $resource->get('link_remote_file')->entity;
    if (!$download_url && $remote_file instanceof FileInterface) {
      $download_url = $remote_file->createFileUrl(FALSE);
      $media_type = $remote_file->getMimeType();
    }

    $api_link_item = $resource->get('link_api')->first();
    if (!$download_url && $api_link_item instanceof LinkItem && !$api_link_item->isEmpty()) {
      $api_link = $api_link_item->getUrl()->setAbsolute(TRUE)->toString(TRUE)->getGeneratedUrl();
    }

    return [
      'downloadURL' => $download_url,
      'mediaType' => $media_type,
      'resourceType' => $resource_type,
      'format' => $format,
      'accessUrl' => $api_link,
      'description' => $resource->get('body')->value,
      'title' => $resource->label(),
    ];
  }

}

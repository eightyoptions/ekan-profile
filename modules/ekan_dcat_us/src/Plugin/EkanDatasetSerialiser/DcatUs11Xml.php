<?php

namespace Drupal\ekan_dcat_us\Plugin\EkanDatasetSerialiser;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Url;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserBase;
use Drupal\file\FileInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * A serialiser to support DCAT 1.1 JSON data.
 *
 * @EkanDatasetSerialiser(
 *    id = "dcat_us_1_1_xml",
 *    label = @Translation("DCAT POD 1.1 XML"),
 *    format = "xml"
 *  )
 */
class DcatUs11Xml extends EkanDatasetSerialiserBase {

  /**
   * {@inheritDoc}
   */
  public function getUrlForDataset(EkanDatasetEntity $dataset): Url {
    return Url::fromRoute("ekan_dcat_us.dataset.{$this->getFormat()}", ['dataset' => $dataset->id()]);
  }

  /**
   * {@inheritDoc}
   */
  public function getMimeType(): string {
    return 'application/rdf+xml';
  }

  /**
   * {@inheritDoc}
   */
  public function serialiseBuild(array $build): string {
    $format = $this->getFormat();
    $encoder = new XmlEncoder([XmlEncoder::ROOT_NODE_NAME => 'rdf:RDF']);
    $normalizer = new GetSetMethodNormalizer();
    $serializer = new Serializer([$normalizer], [$encoder]);
    return $serializer->serialize($build, $format);
  }

  /**
   * Builds the dataset file.
   */
  public function buildDataset(EkanDatasetEntity $dataset): array {

    $keywords = [];
    $tags_field = $dataset->get('tags');
    assert($tags_field instanceof EntityReferenceFieldItemListInterface);
    foreach ($tags_field->referencedEntities() as $term) {
      assert($term instanceof TermInterface);
      $keywords[] = $term->label();
    }

    $themes = [];
    foreach ($dataset->get('pod_theme')->getValue() as $item) {
      $themes[] = $item['value'];
    }

    if (empty($themes)) {
      $topic_field = $dataset->get('topic');
      assert($topic_field instanceof EntityReferenceFieldItemListInterface);
      foreach ($topic_field->referencedEntities() as $term) {
        assert($term instanceof TermInterface);
        $themes[] = $term->label();
      }
    }
    // Get path alias first as dataset->url() not returning path_alias on
    // programmatic entity update process.
    $alias_manager = \Drupal::service('path_alias.manager');
    $alias = $alias_manager->getAliasByPath('/dataset/' . $dataset->id());
    // If no path alias then get the absolute link to the url.
    if (!$alias) {
      $landing_page = $dataset->toUrl('uuid_redirect')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    }
    else {
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $landing_page = $host . $alias;
    }
    if (!$dataset->get('landing_page')->isEmpty()) {
      $landing_page = $dataset->get('landing_page')->getString();
    }

    $contact_name = $dataset->get('uid')->entity ? $dataset->get('uid')->entity->label() : '';
    if (!$dataset->get('contact_name')->isEmpty()) {
      $contact_name = $dataset->get('contact_name')->getString();
    }

    // @todo support for mult-value publisher.
    $publisher = $dataset->get('publisher')->entity;
    if ($publisher instanceof GroupInterface) {
      $publisher = $publisher->label();
    }
    else {
      $publisher = NULL;
    }

    $modified_ts = date('U', $dataset->get('changed')->value);
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('ekan_harvest') && !$dataset->get('harvest_source_modified')->isEmpty()) {
      $modified_ts = $dataset->get('harvest_source_modified')->value;
    }

    $distribution_urls = [];
    foreach ($this->buildDistributions($dataset) as $distribution) {
      $distribution_urls[] = $distribution['dcat:accessURL'];
    }

    $data = [
      '@rdf:about' => $landing_page,
      'dct:title' => $dataset->label(),
      'dct:description' => $dataset->label(),
      'dct:identifier' => $dataset->uuid(),
      'dct:issued' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#date",
        '#' => date('c'),
      ],
      'dct:modified' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#date",
        '#' => date('c', $modified_ts),
      ],
      'dct:accrualPeriodicity' => $dataset->get('frequency')->getString(),
      'dct:spatial' => $dataset->get('spatial')->value,
      'dct:language' => $dataset->language()->getId(),
      'dct:publisher' => $publisher,
      'dcat:contactPoint' => [
        'vcard:Kind' => [
          'vcard:fn' => $contact_name,
          'vcard:hasEmail' => $dataset->get('contact_email')->getString(),
        ],
      ],
      'dcat:keyword' => $keywords,
      'dcat:theme' => $themes,
      'dcat:distribution' => $distribution_urls,
    ];

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function buildResource(EkanResourceEntity $resource): array {
    assert($resource instanceof EkanResourceEntity);
    $upload = $resource->get('upload')->entity;
    $download_url = NULL;
    $media_type = NULL;
    $format = NULL;
    $size_bytes = NULL;

    if ($resource->get('resource_format')->entity) {
      $format = $resource->get('resource_format')->entity->label();
    }

    if ($upload instanceof FileInterface) {
      $download_url = $upload->createFileUrl(FALSE);
      $media_type = $upload->getMimeType();
      $size_bytes = $upload->getSize();
    }

    $remote_file = $resource->get('link_remote_file')->entity;
    if (!$download_url && $remote_file instanceof FileInterface) {
      $download_url = $remote_file->createFileUrl(FALSE);
      $media_type = $remote_file->getMimeType();
    }

    return [
      'dct:title' => $resource->label(),
      'dct:description' => $resource->get('body')->value,
      'dct:issued' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
        '#' => date('c'),
      ],
      'dct:modified' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
        '#' => date('c'),
      ],
      'dct:format' => $format,
      'dcat:accessURL' => ['@rdf:resource' => $resource->toUrl('uuid_redirect')->setAbsolute()->toString()],
      'dcat:downloadURL' => ['@rdf:resource' => $download_url],
      'dcat:mediaType' => $media_type,
      'dcat:byteSize' => $size_bytes,
      'foaf:page' => $resource->toUrl('uuid_redirect')->setAbsolute()->toString(),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $dataset_ids = []): array {
    $base_url = Url::fromRoute('<front>')->setAbsolute()->toString();

    $all_distributions = [];
    $all_dataset_urls = [];

    $datasets = EkanDatasetEntity::loadMultiple($dataset_ids);
    $all_datasets = $this->buildDatasets($dataset_ids);

    foreach ($datasets as $dataset) {

      $all_dataset_urls[] = ['@rdf:resource' => $dataset->toUrl('uuid_redirect')->setAbsolute()->toString()];

      foreach ($this->buildDistributions($dataset) as $resource) {
        $all_distributions[] = $resource;
      }
    }

    return [
      "@xmlns:foaf" => "http://xmlns.com/foaf/0.1/",
      "@xmlns:owl" => "http://www.w3.org/2002/07/owl#",
      "@xmlns:rdfs" => "http://www.w3.org/2000/01/rdf-schema#",
      "@xmlns:rdf" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "@xmlns:dcat" => "http://www.w3.org/ns/dcat#",
      "@xmlns:dct" => "http://purl.org/dc/terms/",
      "@xmlns:adms" => "http://www.w3.org/ns/adms#",
      "@xmlns:dc" => "http://purl.org/dc/elements/1.1/",
      "@xmlns:time" => "http://www.w3.org/2006/time#",
      "@xmlns:dcterms" => "http://purl.org/dc/terms/",
      "@xmlns:vcard" => "http://www.w3.org/2006/vcard/ns#",
      'dcat:Catalog' => [
        'dct:title' => \Drupal::config('system.site')->get('name'),
        'dct:description' => \Drupal::config('system.site')->get('slogan'),
        'foaf:homepage' => Url::fromUri('internal:/search', ['query' => ['f' => ['content_type:dataset']]])->setAbsolute()->toString(),
        'dct:language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
        'dct:issued' => [
          '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
          '#' => date('c'),
        ],
        'dct:modified' => [
          '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
          '#' => date('c'),
        ],
        'dcat:dataset' => $all_dataset_urls,
      ],
      'dcat:Dataset' => $all_datasets,
      'dcat:Distribution' => $all_distributions,
      'foaf:Agent' => [
        '@rdf:about' => $base_url . '/publisher/n0',
        'foaf:name' => 'DKAN',
        'foaf:homepage' => $base_url,
        'dct:type' => [
          '@rdf:resource' => 'http://purl.org/adms/publishertype/NonProfitOrganisation',
        ],
      ],
    ];
  }

}

<?php

namespace Drupal\ekan_migrate\Plugin\migrate\source;

use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Drupal 7 node source from database.
 *
 * @MigrateSource(
 *   id = "d7kan_resource",
 *   source_module = "node"
 * )
 */
class DkanResource extends Node {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->condition('n.type', 'resource');
    $query->leftJoin('node', 'node_base', 'n.nid = node_base.nid');
    $query->addField('node_base', 'uuid');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['uuid'] = $this->t('UUID');
    return $fields;
  }

}

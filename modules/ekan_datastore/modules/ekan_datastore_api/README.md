# EKAN Datastore API

This is port of the old DKAN drupal 7.x dkan_datastore_api module

# Datastore API

DKAN offers a Datastore API as a custom endpoint for the Drupal Services module. This API is designed to be as compatible as possible with the [CKAN Datastore API](https://ckan.readthedocs.org/en/latest/maintaining/datastore.html).

For more information, see the [documentation](https://dkan.readthedocs.io/en/latest/).
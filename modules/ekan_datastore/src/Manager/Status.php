<?php

namespace Drupal\ekan_datastore\Manager;

/**
 * Class Status.
 *
 * Html componet that displays the status of a datastore manager.
 */
class Status {

  /**
   * Private method.
   */
  public static function datastoreStateToString($state) {
    switch ($state) {
      case DatastoreManagerInterface::STORAGE_UNINITIALIZED:
        return t("Uninitialized");

      case DatastoreManagerInterface::STORAGE_INITIALIZED:
        return t("Initialized");

      case DatastoreManagerInterface::DATA_IMPORT_UNINITIALIZED:
        return t("Ready");

      case DatastoreManagerInterface::DATA_IMPORT_READY:
        return t("Ready");

      case DatastoreManagerInterface::DATA_IMPORT_IN_PROGRESS:
        return t("In Progress");

      case DatastoreManagerInterface::DATA_IMPORT_PAUSED:
        return t("Paused");

      case DatastoreManagerInterface::DATA_IMPORT_DONE:
        return t("Done");

      case DatastoreManagerInterface::DATA_IMPORT_ERROR:
        return t("Error");
    }
  }

}

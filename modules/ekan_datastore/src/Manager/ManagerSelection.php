<?php

namespace Drupal\ekan_datastore\Manager;

use Drupal\ekan_datastore\Resource;

/**
 * Class ManagerSelection.
 *
 * Form component that manages the selection of a datastore manager.
 */
class ManagerSelection {

  /**
   * A datastore resource wrapper object.
   *
   * @var \Drupal\ekan_datastore\Resource
   */
  private Resource $resource;

  /**
   * The datastore manager plugin.
   *
   * @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface
   */
  private $datastoreManager;

  /**
   * Constructor.
   */
  public function __construct(Resource $resource, DatastoreManagerInterface $manager) {
    $this->resource = $resource;
    $this->datastoreManager = $manager;
  }

  /**
   * Get form.
   */
  public function getForm() {
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $datastore_manager_plugin_manager */
    $datastore_manager_plugin_manager = \Drupal::service('plugin.manager.datastore_manager');
    $managers_info = $datastore_manager_plugin_manager->getDefinitions();

    $form = [];

    // We only show this if there are multiple managers.
    if (count($managers_info) > 1) {
      $default_plugin_id = key($managers_info);

      $options = [];

      foreach ($managers_info as $plugin_id => $manager_info) {
        $options[$plugin_id] = $manager_info['label'];
      }
      $form['datastore_managers_selection'] = [
        '#type' => 'select',
        '#title' => t('Change datastore importer:'),
        '#options' => $options,
        '#default_value' => $default_plugin_id,
      ];
    }

    return $form;
  }

  /**
   * Submit.
   */
  public function submit($values) {
    $this->datastoreManager->saveState();
  }

}

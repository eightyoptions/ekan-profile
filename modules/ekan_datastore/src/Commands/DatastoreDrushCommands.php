<?php

namespace Drupal\ekan_datastore\Commands;

use Drupal\Core\Site\Settings;
use Drupal\ekan_datastore\DatastoreStorage;
use Drupal\ekan_datastore\LockableDrupalVariables;
use Drush\Drush;

/**
 * Class to provide drush commands for handling datastores.
 */
class DatastoreDrushCommands {

  /**
   * Drop orphan datastore tables if they exist.
   *
   * @command ekan-datastore-drop-orphan-tables
   *
   * @aliases ekan-datastore-dot
   *
   * @usage ekan-datastore-drop-orphan_tables
   */
  public function dropOrphanTables() {
    $tablename_prefix = Settings::get('ekan_datastore_tablename_prefix', 'ekan_datastore_');
    $tablename_prefix = DatastoreStorage::database()->escapeLike($tablename_prefix);
    $query = DatastoreStorage::database()->query("SHOW TABLES LIKE '$tablename_prefix%'");
    $result = $query->fetchAllKeyed(0, 0);
    $entity_ids = [];
    $orphans = 0;
    foreach ($result as $table) {
      if (preg_match("/^$tablename_prefix(?<entity_id>\d+$)/", $table, $matches)) {
        $entity_ids[] = $matches['entity_id'];
      }
    }

    if (!empty($entity_ids)) {
      Drush::output()->writeln("Checking for the following nodes " . implode(', ', $entity_ids));
      $to_truncate = [];
      foreach ($entity_ids as $id) {
        $exists = DatastoreStorage::database()->select('resource')->condition('id', $id)->countQuery()->execute()->fetchField();
        if (!$exists) {
          $to_truncate[] = "{$tablename_prefix}{$id}";
          $orphans++;
        }
      }
      Drush::output()->writeln("Orphan tables found: $orphans");
      foreach ($to_truncate as $table) {
        DatastoreStorage::database()->schema()->dropTable($table);
        Drush::output()->writeln("Deleted orphan table $table");
      }
    }
    else {
      Drush::output()->writeln("There are currently no datastore tables on this site.");
    }
  }

  /**
   * Delete datastore config.
   *
   * Remove datastore store configuration for a resource. (Only use if
   * configuration have been orphaned - The configuration exists but the
   * resource does not). For any other scenario use the datasotre UI.
   *
   * @param int $resource_id
   *   Resource entity id.
   *
   * @command ekan-datastore-delete-config
   *
   * @usage ekan-datastore-delete resource_id
   */
  public function datastoreDeleteConfig($resource_id) {
    $state_storage = new LockableDrupalVariables("ekan_datastore");
    if ($state_storage->get($resource_id)) {
      $state_storage->delete($resource_id);
      Drush::output()->writeln("Datastore config deleted for resource $resource_id");
    }
  }

  /**
   * Remove datastore store configuration and datastore table for a resource.
   *
   * @param int $resource_id
   *   Resource entity id.
   *
   * @command ekan-datastore-delete
   *
   * @usage ekan-datastore-delete resource_id
   */
  public function datastoreDelete($resource_id) {
    $state_storage = new LockableDrupalVariables("ekan_datastore");
    if ($state_storage->get($resource_id)) {
      $this->datastoreDeleteConfig($resource_id);
      $tablename_prefix = Settings::get('ekan_datastore_tablename_prefix', 'ekan_datastore_');
      DatastoreStorage::database()
        ->schema()
        ->dropTable($tablename_prefix . $resource_id);
      Drush::output()->writeln("Datastore table deleted for resource $resource_id");
    }
  }

}

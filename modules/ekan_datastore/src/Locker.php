<?php

namespace Drupal\ekan_datastore;

use Drupal\Core\Site\Settings;

/**
 * Wrapper for the drupal locking service.
 */
class Locker {

  /**
   * Public method.
   */
  public function getLock() {
    $delay = Settings::get('ekan_datastore_lock_delay', 5);
    $timeout = Settings::get('ekan_datastore_lock_timeout', 60);
    $name = 'ekan_datastore_lock';

    $lock = \Drupal::lock()->acquire($name, $timeout)
    || (

      !\Drupal::lock()->wait($name, $delay)
      && \Drupal::lock()->acquire($name, $timeout)
    );
    if (!$lock) {
      \Drupal::logger('ekan_datastore_lock_timeout')->critical("Failed to get datastore variable lock, wait delay exceeded.");
      throw new \Exception("Failed to get datastore variable lock, wait delay exceeded.");
    }
  }

  /**
   * Public method.
   */
  public function releaseLock() {
    $name = 'ekan_datastore_lock';
    \Drupal::lock()->release($name);
  }

}

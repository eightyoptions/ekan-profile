<?php

namespace Drupal\ekan_datastore;

/**
 * Exception when the datastore doesn't support resource mime.
 */
class UnsupportedResourceMimeTypeException extends UnsupportedDatastoreResourceException {

}

<?php

namespace Drupal\ekan_harvest\Plugin\migrate_plus\data_parser;

use Drupal\ekan_harvest\Entity\EkanHarvestSource;
use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\DataParserPluginBase;
use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\XmlTrait;

/**
 * Obtain JSON data for migration.
 *
 * @DataParser(
 *   id = "harvest_source_dcat_xml",
 *   title = @Translation("Harvest Source DCAT XML")
 * )
 */
class HarvestSourceDcatXml extends DataParserPluginBase {

  use CachedHarvestSourceParserTrait;
  use XmlTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $configuration['namespaces'] = array_filter(($configuration['namespaces'] ?? []) + [
      'dcat' => "http://www.w3.org/ns/dcat#",
      'vcard' => "http://www.w3.org/2006/vcard/ns#",
      'dct' => "http://purl.org/dc/terms/",
      'foaf' => "http://xmlns.com/foaf/0.1/",
      'hydra' => "http://www.w3.org/ns/hydra/core#",
      'rdf' => "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      'skos' => "http://www.w3.org/2004/02/skos/core#",
      'locn' => "http://www.w3.org/ns/locn#",
    ]);

    $configuration['dataset_selector'] = $configuration['dataset_selector'] ?? '/rdf:RDF/dcat:Catalog/dcat:dataset/dcat:Dataset';
    $configuration['dataset_field_xpaths'] = array_filter(($configuration['dataset_field_xpaths'] ?? []) + [
      'rights' => 'dct:rights/dct:RightsStatement/skos:prefLabel',
      'accrualPeriodicity' => 'dct:accrualPeriodicity',
      'contactPoint_fn' => 'dcat:contactPoint/*/vcard:fn',
      'contactPoint_hasEmail' => 'dcat:contactPoint/*/vcard:hasEmail/@rdf:resource',
      'identifier' => 'dct:identifier',
      'title' => 'dct:title',
      'description' => 'dct:description',
      'keywords' => 'dcat:keyword',
      'issued' => 'dct:issued',
      'modified' => 'dct:modified',
      'topic' => 'dcat:theme/@rdf:resource',
      'license' => 'dct:license/@rdf:resource',
      'spatial_geographical_cover' => 'dct:spatial_geographical_cover',
      'spatial' => 'dct:spatial/dct:Location/locn:geometry',
      'isPartOf' => 'dct:isPartOf',
      'publisher' => 'dct:publisher/@rdf:resource',
      'references' => 'dct:references',
      'temporal' => 'dcat:temporal',
      'temporal_coverage' => 'dcat:temporal_coverage',
      'landingPage' => '@rdf:about',
      'language' => 'dct:language[1]',
      'distribution' => 'dcat:distribution/@rdf:resource',
    ]);

    $configuration['resource_field_xpaths'] = array_filter(($configuration['resource_field_xpaths'] ?? []) + [
      'identifier' => '@rdf:about',
      'title' => 'dct:title',
      'downloadURL' => 'dcat:downloadURL/@rdf:resource',
      'accessURL' => 'dcat:accessURL/@rdf:resource',
      'format' => 'dct:format',
      'description' => 'dct:description',
      'mediaType' => 'dcat:mediaType',
      'resourceType' => 'dcat:resourceType',
    ]);

    $configuration['data_parser_mode'] = $configuration['data_parser_mode'] ?? 'datasets';

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->harvestSource = EkanHarvestSource::load($this->configuration['harvest_source_id']);
    $this->fileSystem = \Drupal::service('file_system');
  }

  /**
   * Parse XML in to SimpleXML results.
   *
   * @return array|false|null|\SimpleXMLElement
   *   The xml elements if found at this xpath.
   */
  protected function parseXml(string $url): ?\SimpleXMLElement {
    // Clear XML error buffer. Other Drupal code that executed during the
    // migration may have polluted the error buffer and could create false
    // positives in our error check below. We are only concerned with errors
    // that occur from attempting to load the XML string into an object here.
    libxml_clear_errors();

    $xml_data = $this->getDataFetcherPlugin()->getResponseContent($url);
    $xml = simplexml_load_string(trim($xml_data));
    foreach (libxml_get_errors() as $error) {
      $error_string = self::parseLibXmlError($error);
      throw new MigrateException($error_string);
    }

    $this->registerNamespaces($xml);
    return $xml;
  }

  /**
   * Build a php array based on a SimpleXMLElement + xpaths.
   */
  protected function buildResultRecord(\SimpleXMLElement $xml, $field_xpaths): array {
    $record = [];
    foreach ($field_xpaths as $field_name => $xpath) {
      foreach (@$xml->xpath($xpath) ?: [] as $value) {
        if ($value->children() && !trim((string) $value)) {
          $record[$field_name][] = $value;
        }
        else {
          $record[$field_name][] = (string) $value;
        }
      }
    }

    // Reduce single-value results to scalars.
    foreach ($record as $field_name => $values) {
      if (is_array($values) && count($values) == 1) {
        $record[$field_name] = reset($values);
      }
    }

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  protected function openSourceUrl(string $url): bool {
    $local_filename = $this->getCachedFilename($url);
    $dataset_xml_items = @self::parseXml($local_filename)
      ->xpath($this->configuration['dataset_selector']);

    $datasets = [];
    foreach ($dataset_xml_items as $dataset_xml) {
      if ($dataset_xml === FALSE || is_null($dataset_xml)) {
        continue;
      }

      $dataset = $this->buildResultRecord($dataset_xml, $this->configuration['dataset_field_xpaths']);

      $publisher = $dataset['publisher'] ?? NULL;
      if ($publisher) {
        $results = $this->parseXml($local_filename)
          ->xpath("//foaf:Organization[@rdf:about='$publisher']/foaf:name");
        if (is_array($results)) {
          $dataset['publisher_name'] = (string) reset($results);
        }
      }
      $datasets[] = $dataset;
    }

    // Do filters, excludes etc.
    $this->doFilters($datasets, $this->configuration['harvest_filters']);
    $this->doExcludes($datasets, $this->configuration['harvest_excludes']);
    $this->doOverrides($datasets, $this->configuration['harvest_overrides']);
    $this->doDefaults($datasets, $this->configuration['harvest_defaults']);

    if ($this->configuration['data_parser_mode'] == 'resources') {
      $resources = [];
      foreach ($datasets as $dataset) {
        foreach ((array) $dataset['distribution'] as $distribution_id) {
          $result = $this->parseXml($local_filename)
            ->xpath("//dcat:Distribution[@rdf:about='$distribution_id']");
          if (is_array($result)) {
            $resource = $this->buildResultRecord($result[0], $this->configuration['resource_field_xpaths']);
            $resource['dataset_id'] = $dataset['identifier'];
            $resources[] = $resource;
          }
        }
      }
      $this->iterator = new \ArrayIterator($resources);
    }
    else {
      $this->iterator = new \ArrayIterator($datasets);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchNextRow(): void {
    $this->currentItem = $this->iterator->current();
    if ($this->currentItem) {
      $this->iterator->next();
    }
  }

}

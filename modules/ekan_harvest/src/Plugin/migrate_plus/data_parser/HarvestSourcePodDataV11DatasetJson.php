<?php

namespace Drupal\ekan_harvest\Plugin\migrate_plus\data_parser;

use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;

/**
 * Obtain JSON data for migration.
 *
 * @DataParser(
 *   id = "pod_data_v11_dataset_json",
 *   title = @Translation("DCAT 1.1 Datasource Parser")
 * )
 */
class HarvestSourcePodDataV11DatasetJson extends Json {

  use CachedHarvestSourceParserTrait;

  /**
   * {@inheritDoc}
   */
  protected function getSourceData($url, string|int $item_selector = ''): array {
    $datasets = parent::getSourceData($url, $item_selector);

    foreach ($datasets as &$dataset) {

      $spatial = $dataset['spatial'] ?? NULL;

      /** @var \Drupal\geofield\GeoPHP\GeoPHPInterface $geo_php */
      if ($spatial && \Drupal::moduleHandler()->moduleExists('geofield')) {
        $geo_php = \Drupal::service('geofield.geophp');
        if ($geo_php->load($spatial)) {
          continue;
        }

        // If the spatial data looks like 4 decimals,
        // assume it is a bounding box.
        // Match a set of decimals e.g. "32.3974,34.6881,33.5886,35.1704".
        $parts = explode(",", $spatial);
        $parts = array_filter($parts, 'is_numeric');

        if (count($parts) == 4) {
          [$xmin, $ymin, $xmax, $ymax] = $parts;
          // Convert to a polygon to show in the spatial field.
          $geojson_string = json_encode([
            'type' => 'GeometryCollection',
            'geometries' => [
              [
                'type' => 'Polygon',
                'coordinates' => [
                  [
                    [$xmin, $ymin],
                    [$xmax, $ymin],
                    [$xmax, $ymax],
                    [$xmin, $ymax],
                    [$xmin, $ymin],
                  ],
                ],
              ],
            ],
          ]);

          // If this is valid GeoJson.
          if ($geo_php->load($geojson_string)) {
            $dataset['spatial'] = $geojson_string;
          }
        }
      }
    }

    $this->doFilters($datasets, $this->configuration['harvest_filters']);
    $this->doExcludes($datasets, $this->configuration['harvest_excludes']);
    $this->doOverrides($datasets, $this->configuration['harvest_overrides']);
    $this->doDefaults($datasets, $this->configuration['harvest_defaults']);

    return $datasets;
  }

}

<?php

namespace Drupal\ekan_harvest\Plugin\migrate_plus\data_parser;

use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;

/**
 * Obtain JSON data for migration.
 *
 * @DataParser(
 *   id = "harvest_source_json",
 *   title = @Translation("Harvest Source JSON")
 * )
 */
class HarvestSourceJson extends Json {

  use CachedHarvestSourceParserTrait;

  /**
   * {@inheritDoc}
   */
  protected function getSourceData($url, string|int $item_selector = ''): array {
    $local_filename = $this->getCachedFilename($url);

    // Use file_get_contents to load temporary records, not the data fetcher
    // which might be http and does not support temporary://.
    if ($this->currentUrl != $url || !$this->sourceData && parse_url($local_filename, PHP_URL_SCHEME) == 'temporary') {
      $this->sourceData = json_decode(file_get_contents($local_filename), 1);
      $this->currentUrl = $local_filename;
    }

    $datasets = parent::getSourceData($local_filename, $item_selector);
    $this->doFilters($datasets, $this->configuration['harvest_filters']);
    $this->doExcludes($datasets, $this->configuration['harvest_excludes']);
    $this->doOverrides($datasets, $this->configuration['harvest_overrides']);
    $this->doDefaults($datasets, $this->configuration['harvest_defaults']);

    return $datasets;
  }

}

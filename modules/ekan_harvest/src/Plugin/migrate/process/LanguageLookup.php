<?php

namespace Drupal\ekan_harvest\Plugin\migrate\process;

use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Lookup Language.
 *
 * @MigrateProcessPlugin(
 *   id = "language_lookup"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_license:
 *   plugin: language_lookup
 *   source: language
 * @endcode
 */
class LanguageLookup extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (!is_string($value)) {
      return $value;
    }

    foreach (EkanDatasetEntity::languageList() as $language_key => $language) {
      if (trim($value) == $language) {
        return $language_key;
      }
    }

    return $value;
  }

}

<?php

namespace Drupal\ekan_harvest\Plugin\EkanHarvesterType;

/**
 * A Harvester to harvest DCAT-AP XML data.
 *
 * @EkanHarvesterType(
 *   id = "dcat_ap_xml",
 *   label = @Translation("DCAT-AP XML")
 * )
 */
class DcatApXml extends EkanHarvesterTypeBase {

  /**
   * {@inheritDoc}
   */
  public function getMigrationConfigFiles() {
    return [
      __DIR__ . '/migration_config/dcat_ap_xml_datasets.yml',
      __DIR__ . '/migration_config/dcat_ap_xml_resources.yml',
    ];
  }

}

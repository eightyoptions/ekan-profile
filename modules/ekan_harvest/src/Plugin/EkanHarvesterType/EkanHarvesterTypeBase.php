<?php

namespace Drupal\ekan_harvest\Plugin\EkanHarvesterType;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Serialization\Yaml;
use Drupal\ekan_harvest\Entity\EkanHarvestSource;

/**
 * Base class for Ekan Harvester Type plugins.
 */
abstract class EkanHarvesterTypeBase extends PluginBase implements EkanHarvesterTypeInterface {

  /**
   * The harvest source entity.
   *
   * @var \Drupal\ekan_harvest\Entity\EkanHarvestSource
   */
  protected $harvestSource;

  /**
   * {@inheritDoc}
   */
  public function addLanguageDefault(array &$proc_plugin_config): void {
    if ($proc_plugin_config['plugin'] == 'entity_generate') {
      if (!isset($proc_plugin_config['default_values']['langcode'])) {
        $proc_plugin_config['default_values']['langcode'] = $this->harvestSource->get('harvest_langcode')->value;
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getMigrationConfigs() {
    $configs = [];
    foreach ($this->getMigrationConfigFiles() as $path) {
      $config = Yaml::decode(file_get_contents($path));

      // Add the configured harvest_langcode as the default
      // value for entities created during process. e.g. themes, topics etc.
      if ($this->harvestSource) {
        if (!$this->harvestSource->get('harvest_langcode')->isEmpty()) {
          $config['source']['defaults']['harvest_langcode'] = $this->harvestSource->get('harvest_langcode')->value;
        }

        if ($this->harvestSource->get('bypass_validation')->value) {
          $config['destination']['validate'] = FALSE;
        }
      }

      $configs[$config['id']] = $config;
    }

    return $configs;
  }

  /**
   * {@inheritDoc}
   */
  public function getMigrationConfigFiles() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function setHarvestSource(EkanHarvestSource $harvest_source) {
    $this->harvestSource = $harvest_source;
  }

  /**
   * {@inheritDoc}
   */
  public function getHarvestSource(): EkanHarvestSource {
    return $this->harvestSource;
  }

}

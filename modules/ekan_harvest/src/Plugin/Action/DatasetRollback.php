<?php

namespace Drupal\ekan_harvest\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_harvest\Entity\EkanHarvestSource;

/**
 * Redirects to an entity deletion form.
 *
 * @Action(
 *   id = "dataset_rollback",
 *   action_label = @Translation("Rollback - Delete dataset with resources"),
 * )
 */
class DatasetRollback extends ActionBase {

  /**
   * {@inheritDoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermissions($account, [
      'delete any dataset entity',
      'delete any resource entity',
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function execute($object = NULL) {
    self::executeMultiple([$object]);
  }

  /**
   * {@inheritDoc}
   */
  public function executeMultiple(array $entities) {
    $idlist = [];
    $harvest_source = NULL;
    foreach ($entities as $entity) {
      assert($entity instanceof EkanDatasetEntity);
      if (empty($harvest_source)) {
        $harvest_source = $entity->get('harvest_source_ref')->entity;
      }
      $idlist[] = $entity->id();
    }

    if ($harvest_source instanceof EkanHarvestSource) {
      $harvest_source->doRollback($idlist);
    }
  }

}

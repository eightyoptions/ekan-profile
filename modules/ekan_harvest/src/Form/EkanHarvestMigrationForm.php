<?php

namespace Drupal\ekan_harvest\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ekan_harvest\Entity\EkanHarvestSource;

/**
 * Form class for showing the harvester preview.
 */
class EkanHarvestMigrationForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'ekan-harvest-migration-form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EkanHarvestSource $harvest_source = NULL) {
    $form['#harvest_source_id'] = $harvest_source->id();

    $form['update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update Existing'),
      '#description' => $this->t("This is the equivalent of passing --update to a 'drush migrate-import' command, it will update any existing records or re-attempt failed imports"),
    ];

//phpcs:disable
//    $form['limit'] = [
//      '#type' => 'number',
//      '#title' => $this->t('Limit'),
//      '#description' => $this->t("This is the equivalent of passing --limit to a 'drush migrate-import' command, it will only process this many records"),
//    ];
//phpcs:enable

    $form['harvest_now'] = [
      '#type' => 'submit',
      '#value' => t('Harvest Now'),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\ekan_harvest\Entity\EkanHarvestSource $harvest_source */
    Cache::invalidateTags(['migration_plugins']);
    $harvest_source = EkanHarvestSource::load($form['#harvest_source_id']);

    $options = [
      'update' => $form_state->getValue('update', 0),
      // 'force' => 1,
    ];

    $limit = $form_state->getValue('limit');
    if ($limit) {
      $options['limit'] = $limit;
    }

    try {
      $harvest_source->doHarvest($options);
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addError($ex->getMessage());
    }
  }

}

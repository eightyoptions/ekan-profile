<?php

namespace Drupal\ekan_statistics\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\ekan_statistics\ChartBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Ekan statistics controller base.
 */
abstract class EkanStatisticsControllerBase extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Chart builder utility service.
   *
   * @var \Drupal\ekan_statistics\ChartBuilder
   */
  protected ChartBuilder $chartBuilder;

  /**
   * Constructs Ekan Statistics object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The current active database's master connection.
   * @param \Drupal\ekan_statistics\ChartBuilder $chart_builder
   *   A chart builder utility service.
   */
  public function __construct(Connection $database, ChartBuilder $chart_builder) {
    $this->connection = $database;
    $this->chartBuilder = $chart_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('ekan_statistics.chart_builder')
    );
  }

  /**
   * Build function generate statistics page.
   *
   * @return array
   *   Returns renderable array.
   */
  public function build(): array {
    $build = [];
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheTags(['dataset_list', 'resource_list']);
    $cacheability->applyTo($build);
    return $build;
  }

}

<?php

namespace Drupal\ekan_statistics\Controller;

/**
 * Ekan statistics dashboard controller.
 */
class EkanStatisticsDashboard extends EkanStatisticsControllerBase {

  /**
   * Dashboard page build.
   */
  public function build(): array {
    $build = parent::build();
    $groups_count = 10;
    $build['content'] = [
      '#type' => 'inline_template',
      '#context' => [
        'dashboard_resources' => $this->chartBuilder->resourcesByFormatPieChart(),
        'dashboard_licenses' => $this->chartBuilder->datasetsByLicensePieChart(),
        'dashboard_topics' => $this->chartBuilder->datasetsByTopicBarChart(),
        'dashboard_groups' => EkanStatisticsGroups::getGroupsTable($this->connection, $groups_count),
      ],
      '#template' => <<< TWIG
        <div class="container dashboard-wrapper">
          <div class="row dashboard-row-1">
           <div class="col-sm dashboard-resources-block bg-light rounded">
             <h2 class="text-center">Resource count by type</h2>
               {{ dashboard_resources }}
            </div>
          </div>
           <div class="row dashboard-row-2 mt-5">
           <div class="col-sm dashboard-license-block bg-light rounded">
             <h2 class="text-center">Dataset count by licence type</h2>
               {{ dashboard_licenses }}
            </div>
          </div>
          <div class="row dashboard-row-3 mt-5">
           <div class="col-sm dashboard-topics-block bg-light rounded">
             <h2 class="text-center">Dataset count by topic</h2>
               {{ dashboard_topics }}
            </div>
             <div class="col-sm dashboard-groups-block bg-light rounded">
             <h2 class="text-center">Top contributing groups</h2>
               {{ dashboard_groups }}
            </div>
          </div>
          </div>
        </div>
        TWIG,
    ];
    return $build;
  }

}

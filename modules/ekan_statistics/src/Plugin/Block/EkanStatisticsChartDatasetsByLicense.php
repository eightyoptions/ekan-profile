<?php

namespace Drupal\ekan_statistics\Plugin\Block;

/**
 * {@inheritDoc}
 *
 * @Block(
 *   id = "ekan_statistics_chart_datasets_by_license",
 *   admin_label = @Translation("Chart - Datasets by License"),
 *   category = @Translation("EKAN Statistics")
 * )
 */
class EkanStatisticsChartDatasetsByLicense extends EkanStatisticsChartBlockBase {

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $build = [];
    $build['chart'] = $this->chartBuilder->datasetsByLicensePieChart();
    return $build;
  }

}

<?php

namespace Drupal\ekan_statistics\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritDoc}
 *
 * @Block(
 *   id = "ekan_statistics_chart_datasets_by_topic",
 *   admin_label = @Translation("Chart - Datasets by Topic"),
 *   category = @Translation("EKAN Statistics")
 * )
 */
class EkanStatisticsChartDatasetsByTopic extends EkanStatisticsChartBlockBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'chart_type' => 'bar',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['chart_type'] = [
      '#title' => $this->t('Chart Type'),
      '#type' => 'select',
      '#options' => [
        'bar' => $this->t('Bar'),
        'pie' => $this->t('Pie'),
      ],
      '#default_value' => $this->configuration['chart_type'],
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $this->configuration['chart_type'] = $form_state->getValue('chart_type');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $build = [];
    $build['chart'] = $this->chartBuilder->datasetsByTopicChart($this->configuration['chart_type']);
    return $build;
  }

}

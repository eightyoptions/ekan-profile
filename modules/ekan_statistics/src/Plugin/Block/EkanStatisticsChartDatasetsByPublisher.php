<?php

namespace Drupal\ekan_statistics\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritDoc}
 *
 * @Block(
 *   id = "ekan_statistics_chart_datasets_by_publisher",
 *   admin_label = @Translation("Chart - Datasets by Publisher"),
 *   category = @Translation("EKAN Statistics")
 * )
 */
class EkanStatisticsChartDatasetsByPublisher extends EkanStatisticsChartBlockBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'sort' => 'desc',
      'limit' => 0,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['sort'] = [
      '#title' => $this->t('Sort'),
      '#type' => 'select',
      '#options' => [
        'asc' => $this->t('Asc'),
        'desc' => $this->t('Desc'),
      ],
      '#default_value' => $this->configuration['sort'],
    ];
    $form['limit'] = [
      '#title' => $this->t('Limit'),
      '#type' => 'number',
      '#description' => $this->t("0 means no limit"),
      '#default_value' => $this->configuration['limit'],
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $this->configuration['sort'] = $form_state->getValue('sort');
      $this->configuration['limit'] = $form_state->getValue('limit');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $context = [
      'plugin_id' => $this->pluginId,
    ];
    $build = [];
    $build['chart'] = $this->chartBuilder->datasetsByPublisherBarChart($this->configuration, $context);
    return $build;
  }

}

<?php

namespace Drupal\ekan_statistics\Plugin\Block;

/**
 * {@inheritDoc}
 *
 * @Block(
 *   id = "ekan_statistics_chart_resources_by_format",
 *   admin_label = @Translation("Chart - Resources by Format"),
 *   category = @Translation("EKAN Statistics")
 * )
 */
class EkanStatisticsChartResourcesByFormat extends EkanStatisticsChartBlockBase {

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $build = [];
    $build['chart'] = $this->chartBuilder->resourcesByFormatPieChart();
    return $build;
  }

}

<?php

namespace Drupal\ekan_statistics\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ekan_statistics\ChartBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an ouc chart block.
 */
abstract class EkanStatisticsChartBlockBase extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Chart Builder utility service.
   *
   * @var \Drupal\ekan_statistics\ChartBuilder
   */
  protected ChartBuilder $chartBuilder;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $intance = new static($configuration, $plugin_id, $plugin_definition);
    $intance->chartBuilder = $container->get('ekan_statistics.chart_builder');
    return $intance;
  }

  /**
   * {@inheritDoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, 'view ekan statistics');
  }

}

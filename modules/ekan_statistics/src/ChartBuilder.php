<?php

namespace Drupal\ekan_statistics;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\group\Entity\Group;
use Drupal\taxonomy\Entity\Term;

/**
 * Utility class to provide charts to the dashboard controller and blocks.
 */
class ChartBuilder {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The core entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  private EntityRepositoryInterface $entityRepository;

  /**
   * Constructs Ekan Statistics object.
   */
  public function __construct(Connection $database, EntityRepositoryInterface $entity_repository) {
    $this->connection = $database;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Builds the dashboard resource pie chart.
   */
  public function resourcesByFormatPieChart(): array {
    // @todo add ekan_statistics_chart_pre_build alter hook.
    $query = $this->connection->select('resource_field_data', 'res');
    $query->fields('res', ['resource_format']);
    $query->addExpression('COUNT(*)', 'resource_count');
    $query->join('taxonomy_term_field_data', 'term_data', 'term_data.tid = res.resource_format');
    $query->fields('term_data', ['name', 'tid']);
    $query->condition('res.status', 1);
    $query->groupBy('res.resource_format');
    $query->orderBy('resource_count', 'DESC');
    $query->distinct();
    $results = $query->execute()
      ->fetchAllAssoc('resource_format', \PDO::FETCH_ASSOC);
    $data = [];

    $total = 0;
    foreach ($results as $tid => $result) {
      $total += $result['resource_count'];
    }

    foreach ($results as $tid => $result) {
      $term = Term::load($tid);

      $count = $result['resource_count'];
      $data[] = [
        'name' => "{$term->label()} - {$count}",
        'value' => round(($count / $total) * 100, 1),
      ];
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['resource-pie-chart'],
        'id' => 'dashboard-resource-chart-container',
        'style' => 'height:400px;',
      ],
      '#attached' => [
        'library' => ['ekan_statistics/dashboard_resource_pie_chart'],
        'drupalSettings' => [
          'dashboard_resource' => [
            'data' => $data,
          ],
        ],
      ],
      '#cache' => [
        'tags' => ['resource_list', 'taxonomy_term_list:format'],
      ],
    ];
  }

  /**
   * Builds the dashboard dataset pie chart.
   */
  public function datasetsByLicensePieChart(): array {
    // @todo add ekan_statistics_chart_pre_build alter hook.
    $query = $this->connection->select('dataset_field_data', 'dataset');
    $query->fields('dataset', ['license']);
    $query->addExpression('COUNT(*)', 'license_count');
    $query->condition('dataset.status', 1);
    $query->groupBy('license');
    $query->orderBy('license', 'DESC');
    $query->distinct();
    $results = $query->execute()->fetchAllKeyed();
    $data = [];
    $licenses = \Drupal::service('ekan_core.license_handler');
    $licenses_info = $licenses->getLicenseOptions();

    $total = array_sum($results);
    arsort($results);

    foreach ($results as $license => $count) {
      if (empty($license)) {
        $license_label = t('Not Set');
      }
      else {
        $license_label = $licenses_info[$license]['label'] ?? $license;
      }

      $data[] = [
        'name' => "$license_label - $count",
        'value' => round(($count / $total) * 100, 1),
      ];
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['license-pie-chart'],
        'id' => 'dashboard-license-chart-container',
        'style' => 'height:400px;',
      ],
      '#attached' => [
        'library' => ['ekan_statistics/dashboard_license_pie_chart'],
        'drupalSettings' => [
          'dashboard_license' => [
            'data' => $data,
          ],
        ],
      ],
      '#cache' => [
        'tags' => ['dataset_list'],
      ],
    ];
  }

  /**
   * Builds the dashboard number of datasets per topic bar chart.
   */
  public function datasetsByTopicBarChart(): array {
    // @todo add ekan_statistics_chart_pre_build alter hook.
    $query = $this->connection->select('dataset__topic', 'd_top');
    $query->fields('d_top', ['topic_target_id']);
    $query->addExpression('COUNT(*)', 'topic_count');
    $query->join('taxonomy_term_field_data', 'term_data', 'term_data.tid = d_top.topic_target_id');
    $query->fields('term_data', ['name', 'tid']);
    $query->condition('d_top.bundle', 'dataset');
    $query->condition('d_top.deleted', 0);
    $query->groupBy('d_top.topic_target_id');
    $query->orderBy('topic_count', 'DESC');
    $query->distinct();
    $results = $query->execute()->fetchAllAssoc('topic_target_id');
    $topics = [];
    $topics_count = [];
    foreach ($results as $result) {
      $term = Term::load($result->tid);
      $term = $this->entityRepository->getTranslationFromContext($term);
      $topics[] = $term->label();
      $topics_count[] = $result->topic_count;
    }
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['statistics-topics-chart'],
        'id' => 'dashboard-topics-chart-container',
        'style' => 'height:600px;',
      ],
      '#attached' => [
        'library' => ['ekan_statistics/dashboard_topics_bar_chart'],
        'drupalSettings' => [
          'dashboard_topics' => [
            'data' => $topics_count,
            'topics' => $topics,
          ],
        ],
      ],
      '#cache' => [
        'tags' => ['dataset_list', 'taxonomy_term_list:topic'],
      ],
    ];
  }

  /**
   * Builds the dashboard number of datasets per topic bar chart.
   */
  public function datasetsByTopicChart($chart_type = 'bar'): array {
    // @todo add ekan_statistics_chart_pre_build alter hook.
    $query = $this->connection->select('dataset__topic', 'd_top');
    $query->fields('d_top', ['topic_target_id']);
    $query->addExpression('COUNT(*)', 'topic_count');
    $query->join('taxonomy_term_field_data', 'term_data', 'term_data.tid = d_top.topic_target_id');
    $query->fields('term_data', ['name', 'tid']);
    $query->condition('d_top.bundle', 'dataset');
    $query->condition('d_top.deleted', 0);
    $query->groupBy('d_top.topic_target_id');
    $query->orderBy('topic_count', 'DESC');
    $query->distinct();
    $results = $query->execute()->fetchAllAssoc('topic_target_id');
    $topics = [];
    $topics_count = [];

    $pie_data = [];

    $total = 0;
    foreach ($results as $result) {
      $total += $result->topic_count;
    }

    foreach ($results as $result) {
      $term = Term::load($result->tid);
      $term = $this->entityRepository->getTranslationFromContext($term);
      $topics[] = $term->label();
      $topics_count[] = $result->topic_count;

      $pie_data[] = [
        'value' => round(($result->topic_count / $total) * 100, 0),
        'name' => $term->label(),
      ];
    }

    $options = [];

    $options['tooltip'] = [
      'trigger' => 'axis',
      'axisPointer' => [
        'type' => 'shadow',
      ],
    ];
    $options['grid']['containLabel'] = TRUE;

    if ($chart_type == 'pie') {
      $series = [
        "type" => 'pie',
        "data" => $pie_data,
        'radius' => '80%',
        'top' => 0,
        'right' => '33%',
        "label" => [
          "position" => 'inner',
          "formatter" => '{c}%',
        ],
      ];
      $options['series'][] = NestedArray::mergeDeep(self::seriesDefaults(), $series);

      $options['legend']['orient'] = 'vertical';
      $options['legend']['top'] = 0;
      $options['legend']['left'] = "66%";
      $options['legend']['type'] = 'scroll';
    }
    else {
      $series = [
        "type" => 'bar',
        "data" => $topics_count,
      ];
      $options['series'][] = NestedArray::mergeDeep(self::seriesDefaults(), $series);
      $options['yAxis']['type'] = 'value';
      $options['xAxis'] = [
        'data' => $topics,
      ];
    }

    $id = Html::getUniqueId(__FUNCTION__);

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$id],
        'id' => [$id],
        'style' => 'height: 400px;',
      ],
      '#attached' => [
        'library' => ['ekan_core/generic-echart'],
        'drupalSettings' => [
          'generic_echart' => [
            $id => $options,
          ],
        ],
      ],
    ];
  }

  /**
   * Build a chart of datasets by publishers.
   */
  public function datasetsByPublisherBarChart($configuration, $context) {
    \Drupal::moduleHandler()->alter('ekan_statistics_chart_pre_build', $configuration, $context);
    $sort = $configuration['sort'] ?? 'desc';
    $query = $this->connection->select('group_relationship_field_data', 'grfd')
      ->condition('type', 'publisher-dataset')
      ->groupBy('gid');
    $query->addField('grfd', 'gid');
    $query->addExpression('COUNT(entity_id)', 'count');
    $query->orderBy('count', $sort);
    if (isset($configuration['limit']) && intval($configuration['limit']) !== 0) {
      $query->range(0, $configuration['limit']);
    }

    $results = $query->execute()->fetchAllKeyed();
    if ($sort == 'desc') {
      asort($results, SORT_NUMERIC);
    }
    else {
      arsort($results, SORT_NUMERIC);
    }

    $x_axis_data = [];
    $series_data = [];

    foreach (Group::loadMultiple(array_keys($results)) as $group) {
      $group = $this->entityRepository->getTranslationFromContext($group);
      $x_axis_data[] = $group->label();
      $series_data[] = $results[$group->id()];
    }

    $options = [
      'color' => ['#145b98', '#002245'],
    ];

    $options['tooltip'] = [
      'trigger' => 'axis',
      'axisPointer' => [
        'type' => 'shadow',
      ],
    ];
    $options['grid']['containLabel'] = TRUE;

    $series = [
      "type" => 'bar',
      "data" => $series_data,
      'label' => [
        'show' => TRUE,
        'position' => 'right',
      ],
    ];

    $options['series'][] = NestedArray::mergeDeep(self::seriesDefaults(), $series);

    $options['yAxis'] = [
      'data' => $x_axis_data,
      'axisLabel' => [
        'overflow' => 'break',
        'interval' => 0,
        'width' => '150',
      ],
    ];

    $options['xAxis'] = [
      'type' => 'value',
      'axisLabel' => [
        'show' => FALSE,
      ],
    ];

    $id = Html::getUniqueId(__FUNCTION__);

    $height = max(count($series_data) * 48, 250);
    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$id],
        'id' => [$id],
        'style' => "height: {$height}px",
      ],
      '#attached' => [
        'library' => ['ekan_core/generic-echart'],
        'drupalSettings' => [
          'generic_echart' => [
            $id => $options,
          ],
        ],
      ],
    ];
  }

  /**
   * Default settings to apply to series data.
   */
  public function seriesDefaults() {
    return [
      'emphasis' => [
        'itemStyle' => [
          'shadowBlur' => 10,
          'shadowOffsetX' => 0,
          'shadowColor' => 'rgba(0, 0, 0, 0.5)',
        ],
      ],
    ];
  }

}

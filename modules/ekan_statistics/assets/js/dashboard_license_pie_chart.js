(function ($, Drupal, once) {
  Drupal.behaviors.dashboard_license = {
    attach: function (context, settings) {
      once('dashboard_license_pie_chart', document.getElementById('dashboard-license-chart-container'), context).forEach(function (element) {
        let chart = echarts.init(document.getElementById('dashboard-license-chart-container'));
        chart.setOption(buildDashboardLicenseChartOptions());
        window.addEventListener('resize', function () {
          chart.resize();
        });
      });
    },
  };
})(jQuery, Drupal, once);

function buildDashboardLicenseChartOptions() {
  return {
    tooltip: {
      trigger: 'item',
      axisPointer: {
        type: 'shadow',
      },
      formatter: function (params) {
        return `<strong>${params.name}:</strong><br />${params.data.value} datasets`;
      },
    },
    legend: {
      orient: 'vertical',
      top: 0,
      left: '66%',
      type: 'scroll',
      textStyle: {
        overflow: 'break',
        width: 150,
      },
    },
    grid: {
      containLabel: true,
    },
    series: [
      {
        type: 'pie',
        top: 0,
        radius: '80%',
        right: '33%',
        data: drupalSettings.dashboard_license.data,
        label: {
          position: 'inner',
          bleedMargin: 0,
          formatter: '{c}%',
        },
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)',
          },
          label: {
            fontWeight: 'bold',
          },
        },
      },
    ],
  };
}

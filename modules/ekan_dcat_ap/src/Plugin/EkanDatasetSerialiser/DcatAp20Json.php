<?php

namespace Drupal\ekan_dcat_ap\Plugin\EkanDatasetSerialiser;

/**
 * A serialiser to support DCAT-AP 2.0 JSON data.
 *
 * @EkanDatasetSerialiser(
 *    id = "dcat_ap_2_0_json",
 *    label = @Translation("DCAT-AP 2.0 JSON"),
 *    format = "json"
 *  )
 */
class DcatAp20Json extends DcatAp20 {

}

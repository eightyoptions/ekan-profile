<?php

namespace Drupal\ekan_dcat_ap\Plugin\EkanDatasetSerialiser;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Url;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserBase;
use Drupal\file\FileInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Base class for DCAT AP 2.0 serialisers.
 */
abstract class DcatAp20 extends EkanDatasetSerialiserBase {

  /**
   * {@inheritDoc}
   */
  public function getUrlForDataset(EkanDatasetEntity $dataset): ?Url {

    if (!$dataset->id()) {
      return NULL;
    }

    return Url::fromRoute("ekan_dcat_ap.dataset.{$this->getFormat()}", ['dataset' => $dataset->id()]);
  }

  /**
   * {@inheritDoc}
   */
  public function getMimeType(): string {
    $format = $this->getFormat();
    if ($format == 'xml') {
      return 'application/rdf+xml';
    }
    elseif ($format == 'json') {
      return 'application/json';
    }
    else {
      throw new \Exception("Format $format is not supported by this builder");
    }
  }

  /**
   * {@inheritDoc}
   */
  public function serialiseBuild(array $build): string {
    $format = $this->getFormat();

    if ($format == 'xml') {
      $encoder = new XmlEncoder([XmlEncoder::ROOT_NODE_NAME => 'rdf:RDF']);
    }
    elseif ($format == 'json') {
      $encoder = new JsonEncoder();
    }
    else {
      throw new \Exception("$format is not supported for serialization");
    }

    // Remove any elements with null values.
    $build = self::filterEmptyValues($build);

    $normalizer = new GetSetMethodNormalizer();
    $serializer = new Serializer([$normalizer], [$encoder]);
    return $serializer->serialize($build, $format);
  }

  /**
   * {@inheritDoc}
   */
  public function buildResource(EkanResourceEntity $resource): array {

    $upload = $resource->get('upload')->entity;
    $download_url = NULL;
    $media_type = NULL;
    $format = NULL;
    $size_bytes = NULL;

    if ($resource->get('resource_format')->entity) {
      $format = $resource->get('resource_format')->entity->label();
    }

    if ($upload instanceof FileInterface) {
      $download_url = $upload->createFileUrl(FALSE);
      $media_type = $upload->getMimeType();
      $size_bytes = $upload->getSize();
    }

    $remote_file = $resource->get('link_remote_file')->entity;
    if (!$download_url && $remote_file instanceof FileInterface) {
      $download_url = $remote_file->createFileUrl(FALSE);
      $media_type = $remote_file->getMimeType();
    }

    $languages = [];
    foreach ($resource->getTranslationLanguages() as $language) {
      $languages[] = [
        'dct:LinguisticSystem' => [
          'rdf:language' => $language->getId(),
        ],
      ];
    }

    return [
      'dct:title' => $resource->label(),
      'dct:description' => $resource->get('body')->value,
      'dct:issued' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
        '#' => date('c'),
      ],
      'dct:modified' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
        '#' => date('c'),
      ],
      'dct:format' => [
        'dct:MediaTypeOrExtent' => [
          'rdf:value' => $format,
        ],
      ],
      'dct:language' => $languages,
      'dcat:accessURL' => ['@rdf:resource' => $resource->toUrl('uuid_redirect')->setAbsolute()->toString()],
      'dcat:downloadURL' => ['@rdf:resource' => $download_url],
      'dcat:mediaType' => [
        'dct:MediaType' => [
          'rdf:value' => $media_type,
        ],
      ],
      'dcat:byteSize' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#decimal",
        '#' => $size_bytes,
      ],
      'foaf:page' => [
        'foaf:Document' => [
          'rdf:value' => $resource->toUrl('uuid_redirect')->setAbsolute()->toString(),
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildDataset(EkanDatasetEntity $dataset): array {

    $keywords = [];
    $tags_field = $dataset->get('tags');
    assert($tags_field instanceof EntityReferenceFieldItemListInterface);
    foreach ($tags_field->referencedEntities() as $term) {
      assert($term instanceof TermInterface);
      $keywords[] = $term->label();
    }

    $themes = [];
    foreach ($dataset->get('pod_theme')->getValue() as $item) {
      $themes[] = [
        'skos:Concept' => [
          'skos:prefLabel' => $item['value'],
          'rdf:value' => $item['value'],
        ],
      ];
    }

    if (empty($themes)) {
      $topic_field = $dataset->get('topic');
      assert($topic_field instanceof EntityReferenceFieldItemListInterface);
      foreach ($topic_field->referencedEntities() as $term) {
        assert($term instanceof TermInterface);
        $themes[] = [
          'skos:Concept' => [
            'skos:prefLabel' => $term->label(),
            'rdf:value' => $term->label(),
          ],
        ];
      }
    }

    if (!$dataset->get('landing_page')->isEmpty()) {
      $landing_page = $dataset->get('landing_page')->getString();
    }
    else {
      $landing_page = $dataset->toUrl('uuid_redirect')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    }

    $contact_name = $dataset->get('uid')->entity ? $dataset->get('uid')->entity->label() : '';
    if (!$dataset->get('contact_name')->isEmpty()) {
      $contact_name = $dataset->get('contact_name')->getString();
    }

    $publishers = [];
    foreach ($dataset->getGroups() as $publisher) {
      $publishers[] = [
        'foaf:Agent' => [
          'foaf:name' => $publisher->label(),
        ],
      ];
    }

    $languages = [];
    foreach ($dataset->getTranslationLanguages() as $language) {
      $languages[] = [
        'dct:LinguisticSystem' => [
          'rdf:language' => $language->getId(),
        ],
      ];
    }

    $modified_ts = date('U', $dataset->get('changed')->value);
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('ekan_harvest') && !$dataset->get('harvest_source_modified')->isEmpty()) {
      $modified_ts = $dataset->get('harvest_source_modified')->value;
    }

    $distribution_urls = [];
    foreach ($this->buildDistributions($dataset) as $distribution) {
      $distribution_urls[] = $distribution['dcat:accessURL'];
    }

    $data = [
      '@rdf:about' => $landing_page,
      'dct:title' => $dataset->label(),
      'dct:description' => $dataset->label(),
      'dct:identifier' => $dataset->uuid(),
      'dct:issued' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#date",
        '#' => date('c'),
      ],
      'dct:modified' => [
        '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#date",
        '#' => date('c', $modified_ts),
      ],
      'dct:accrualPeriodicity' => [
        'dct:Frequency' => [
          'rdf:value' => $dataset->get('frequency')->getString(),
        ],
      ],
      'dct:spatial' => [
        'dct:Location' => [
          'locn:geometry' => [
            '@rdf:datatype' => "http://www.opengis.net/ont/geosparql#asWKT",
            '#' => $dataset->get('spatial')->value,
          ],
        ],
      ],
      'dct:language' => $languages,
      'dct:publisher' => $publishers,
      'dcat:contactPoint' => [
        'vcard:Kind' => [
          'vcard:fn' => $contact_name,
          'vcard:hasEmail' => $dataset->get('contact_email')->getString(),
        ],
      ],
      'dcat:keyword' => $keywords,
      'dcat:theme' => $themes,
      'dcat:Distribution' => $distribution_urls,
    ];

    return self::filterEmptyValues($data);
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $dataset_ids = []): array {
    $base_url = Url::fromRoute('<front>')->setAbsolute()->toString();

    $all_distributions = [];

    $datasets = EkanDatasetEntity::loadMultiple($dataset_ids);
    $all_datasets = [];
    foreach ($this->buildDatasets($dataset_ids) as $dataset) {
      $all_datasets['dcat:dataset'][] = ['dcat:Dataset' => $dataset];
    };

    foreach ($datasets as $dataset) {
      foreach ($this->buildDistributions($dataset) as $resource) {
        $all_distributions[] = $resource;
      }
    }

    $languages = [];
    foreach (\Drupal::languageManager()->getNativeLanguages() as $language) {
      $languages[] = [
        'dct:LinguisticSystem' => [
          'rdf:value' => $language->getId(),
        ],
      ];
    }

    $publisher = \Drupal::config('ekan_core.dataset_api_settings')->get('default_publisher') ?: \Drupal::config('system.site')->get('name');

    return [
      "@xmlns:foaf" => "http://xmlns.com/foaf/0.1/",
      "@xmlns:owl" => "http://www.w3.org/2002/07/owl#",
      "@xmlns:rdfs" => "http://www.w3.org/2000/01/rdf-schema#",
      "@xmlns:rdf" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "@xmlns:dcat" => "http://www.w3.org/ns/dcat#",
      "@xmlns:dct" => "http://purl.org/dc/terms/",
      "@xmlns:adms" => "http://www.w3.org/ns/adms#",
      "@xmlns:dc" => "http://purl.org/dc/elements/1.1/",
      "@xmlns:time" => "http://www.w3.org/2006/time#",
      "@xmlns:dcterms" => "http://purl.org/dc/terms/",
      "@xmlns:vcard" => "http://www.w3.org/2006/vcard/ns#",
      '@xmlns:skos' => "http://www.w3.org/2004/02/skos/core#",
      '@xmlns:locn' => "http://www.w3.org/ns/locn#",
      'dcat:Catalog' => [
        'dct:title' => \Drupal::config('system.site')->get('name'),
        'dct:description' => \Drupal::config('system.site')->get('slogan'),
        'foaf:homepage' => [
          'foaf:Document' => [
            'rdf:value' => Url::fromUri('internal:/search', ['query' => ['f' => ['content_type:dataset']]])->setAbsolute()->toString(),
          ],
        ],
        'dct:language' => $languages,
        'dct:issued' => [
          '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
          '#' => date('c'),
        ],
        'dct:modified' => [
          '@rdf:datatype' => "http://www.w3.org/2001/XMLSchema#dateTime",
          '#' => date('c'),
        ],
        'dct:publisher' => [
          'foaf:Agent' => [
            'foaf:name' => $publisher,
          ],
        ],
      ] + $all_datasets,
      'dcat:Distribution' => $all_distributions,
      'foaf:Agent' => [
        '@rdf:about' => $base_url . '/publisher/n0',
        'foaf:name' => 'DKAN',
        'foaf:homepage' => $base_url,
        'dct:type' => [
          'skos:Concept' => [
            'skos:prefLabel' => 'http://purl.org/adms/publishertype/NonProfitOrganisation',
            'rdf:value' => 'http://purl.org/adms/publishertype/NonProfitOrganisation',
          ],
        ],
      ],
    ];
  }

}

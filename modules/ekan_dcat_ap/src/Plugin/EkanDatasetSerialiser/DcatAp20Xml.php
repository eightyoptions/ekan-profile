<?php

namespace Drupal\ekan_dcat_ap\Plugin\EkanDatasetSerialiser;

/**
 * A serialiser to support DCAT-AP 2.0 XML data.
 *
 * @EkanDatasetSerialiser(
 *    id = "dcat_ap_2_0_xml",
 *    label = @Translation("DCAT-AP 2.0 XML"),
 *    format = "xml"
 *  )
 */
class DcatAp20Xml extends DcatAp20 {

}

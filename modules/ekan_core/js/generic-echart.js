(function (Drupal) {
  Drupal.behaviors.generic_echart = {
    attach: function (context, settings) {
      Object.keys(settings.generic_echart).forEach(function (key) {
        var chart_options = settings.generic_echart[key];

        // Skip init if chart is not visible.
        var el = document.getElementById(key);
        if (el.offsetParent === null) {
          return;
        }

        var chart = echarts.init(el);
        chart.setOption(chart_options);

        window.addEventListener('resize', function () {
          chart.resize();
        });
      });
    }
  };
})(Drupal);

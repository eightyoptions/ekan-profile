// Ported from DKAN.
(function ($, Drupal, once) {
  Drupal.behaviors.ekan_dataset = {
    attach: function (context, settings) {
      once('ekan_dataset', 'html').forEach(function (element) {
        var height = 150;
        var dif = 30;
        var offset = height - dif;
        var id =  'table.cols-1 > tbody  > tr';
        ekanDatasetRowHide(id, height, dif, offset);
      });
    }
  }

  // Adds "Show more" to rows longer than height.
  function ekanDatasetRowHide(id, height, dif, offset) {
    $(id).each(function() {
      if ($(this).height() > height) {
        height = height + "px";
        offset  = offset + "px";
        $(this).find('td.views-field').append('<div style="position: absolute;top: ' + offset + ';width: 100%;background-color: #FFF;height: ' + dif + 'px;text-align: center;opacity: 0.9;" id="click-full"><a href="#">' + Drupal.t("Show more") + '</a></div>');
        $(this).find('td.views-field').eq(0).css({"height": height, "overflow":"hidden", "position":"relative", "display":"block", "padding-bottom" : dif});
      }
    });

    $('#click-full').click(function (event) {
      event.preventDefault(); // Prevent the default behavior of the link
      if ($(this).hasClass("clicked")) {
        $(this).parent().css({"height": height, "overflow":"hidden"});
        $(this).css({"bottom": "inherit", "top" : offset, "padding" : "0 0 20px 0"});
        $(this).find("a").text(Drupal.t("Show more"));
        $(this).removeClass("clicked");
      }
      else {
        $(this).parent().css({"height": "inherit"});
        $(this).css({"bottom": "0", "top" : "inherit", "padding" : "0 0 20px 0"});
        $(this).find("a").text(Drupal.t("hide"));
        $(this).addClass("clicked");
      }
      return false;
    });
  }

})(jQuery, Drupal, once);

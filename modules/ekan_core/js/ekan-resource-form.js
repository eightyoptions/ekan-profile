(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.ekanResourceForm = {
    attach: function (context, settings) {
      once('fieldgroup_effects', 'details.horizontal-tabs-pane', context).forEach(function (el) {
        // Make field group horizontal tab with any value in it focus.
        if ($(el).find('input').val()) {
          $(el).data('horizontalTab').focus();
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, once);

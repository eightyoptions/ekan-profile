<?php

/**
 * @file
 * Token for ekan entities.
 */

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Entity\EkanEntityBase;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * Implements hook_token_info().
 */
function ekan_core_token_info() {
  $type = [
    'name' => t('Ekan Entities'),
    'description' => t('Tokens for Ekan Entities.'),
  ];
  $ekan['label'] = [
    'name' => t("Resource or Dataset label"),
  ];
  $ekan['type'] = [
    'name' => t("Entity Type"),
  ];
  $ekan['url'] = [
    'name' => t("Url"),
    'type' => 'url',
  ];
  return [
    'types' => ['ekan_entity' => $type],
    'tokens' => ['ekan_entity' => $ekan],
  ];
}

/**
 * Implements hook_tokens().
 */
function ekan_core_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  // Given an resource or dataset, build tokens.
  $build_common_replacements = function (EkanEntityBase $entity) use ($tokens, $options, $bubbleable_metadata) {
    $common_replacements = [];

    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');

    $url_options = [];
    if (isset($options['langcode'])) {
      $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
      $langcode = $options['langcode'];
    }
    else {
      $langcode = LanguageInterface::LANGCODE_DEFAULT;
    }
    $translation = $entity_repository->getTranslationFromContext($entity, $langcode, ['operation' => 'ekan_tokens']);

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'label':
          $common_replacements[$original] = $translation->label();
          break;

        case 'type':
          $common_replacements[$original] = $entity->getEntityType()->getLabel();
          break;

        case 'url':
          $result = $entity->toUrl('canonical', $url_options)->toString(TRUE);
          $bubbleable_metadata->addCacheableDependency($result);
          $common_replacements[$original] = $result->getGeneratedUrl();
          break;
      }
    }

    if ($url_tokens = \Drupal::token()->findWithPrefix($tokens, 'url')) {
      $common_replacements += \Drupal::token()->generate('url', $url_tokens, ['url' => $entity->toUrl('canonical', $url_options)], $options, $bubbleable_metadata);
    }

    return $common_replacements;
  };

  $replacements = [];
  if ($type == 'ekan_entity') {
    if (isset($data['dataset']) && $data['dataset'] instanceof EkanDatasetEntity) {
      $replacements = $build_common_replacements($data['dataset']);
    }
    if (isset($data['resource']) && $data['resource'] instanceof EkanResourceEntity) {
      $replacements = $build_common_replacements($data['resource']);
    }
  }
  return $replacements;
}

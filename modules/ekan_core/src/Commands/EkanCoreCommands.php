<?php

namespace Drupal\ekan_core\Commands;

use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserInterface;

/**
 * Drush commands for building ekan data.json.
 */
class EkanCoreCommands {

  /**
   * Perform data.json data build.
   *
   * @command ekan:data_build
   *
   * @default $options []
   *
   * @option force
   *  Force the data to rebuild, regardless of cache status.
   *
   * @usage ekan:data_build
   *   Build data.json
   */
  public function dataBuild($options = ['force' => FALSE]) {
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.ekan_dataset_serialiser');
    foreach ($manager->getDefinitions() as $plugin_id => $definition) {
      $instance = $manager->createInstance($plugin_id);
      assert($instance instanceof EkanDatasetSerialiserInterface);

      if ($options['force']) {
        $instance->invalidateCache();
      }

      if ($instance->isCacheStale()) {
        $filepath = $instance->warmCache();
        \Drupal::logger('ekan_core')->info("Generated $filepath");
      }
    }
  }

}

<?php

namespace Drupal\ekan_core\Routing;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Site\Settings;
use Drupal\ekan_core\Controller\DatasetSerialisedDataController;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alters existing routes, provides new ones.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.ekan_dataset_serialiser');

    // Set up a route for /data.json.
    $data_json_plugin = Settings::get('ekan_core')['data.json.plugin_id'] ?? 'dcat_us_1_1_json';
    try {
      $plugin = $manager->createInstance($data_json_plugin);
      if ($plugin instanceof EkanDatasetSerialiserInterface && $plugin instanceof PluginBase) {
        $route = new Route('data.json', [
          '_controller' => DatasetSerialisedDataController::class . '::retrieveAll',
          'plugin_id' => $plugin->getPluginId(),
        ], [
          '_permission' => 'access content',
        ]);
        $collection->add("{$plugin->getPluginId()}.data.json", $route);
      }
    }
    catch (\Exception $ex) {
      \Drupal::logger('ekan_core')->info($ex->getMessage());
    }

    // Set up a route for /catalog.xml.
    $catalog_xml_plugin = Settings::get('ekan_core')['catalog.xml.plugin_id'] ?? 'dcat_us_1_1_xml';
    try {
      $plugin = $manager->createInstance($catalog_xml_plugin);
      if ($plugin instanceof EkanDatasetSerialiserInterface && $plugin instanceof PluginBase) {
        $route = new Route('catalog.xml', [
          '_controller' => DatasetSerialisedDataController::class . '::retrieveAll',
          'plugin_id' => $plugin->getPluginId(),
        ], [
          '_permission' => 'access content',
        ]);
        $collection->add("{$plugin->getPluginId()}.catalog.xml", $route);
      }
    }
    catch (\Exception $ex) {
      \Drupal::logger('ekan_core')->info($ex->getMessage());
    }

  }

}

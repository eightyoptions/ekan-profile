<?php

namespace Drupal\ekan_core\Service;

/**
 * Interface for licence handler services.
 */
interface EkanLicenseHandlerInterface {

  /**
   * Return the license array including name and url.
   */
  public function getLicenseOptions();

  /**
   * Return the allowed values for select lists.
   */
  public function getAllowedValues();

  /**
   * Return the license array.
   */
  public function getLicenseFromMachineName($machineName);

  /**
   * Return the license array.
   */
  public function getLicenseFromUri($uri);

}

<?php

namespace Drupal\ekan_core\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\ekan_core\Annotation\EkanDatasetSerialiser;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserInterface;

/**
 * Manages discovery and instantiation of Ekan Harvester Type plugins.
 */
class EkanDatasetSerialiserManager extends DefaultPluginManager {

  /**
   * {@inheritDoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {

    parent::__construct(
      'Plugin/EkanDatasetSerialiser',
      $namespaces,
      $module_handler,
      EkanDatasetSerialiserInterface::class,
      EkanDatasetSerialiser::class
    );

    $this->alterInfo('ekan_dataset_serialiser_info');
    $this->setCacheBackend($cache_backend, 'ekan_dataset_serialiser_plugins');
  }

}

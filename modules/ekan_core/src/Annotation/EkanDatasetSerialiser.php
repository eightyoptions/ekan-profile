<?php

namespace Drupal\ekan_core\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for plugins of type EkanDatasetSerialiser.
 *
 * @Annotation
 */
class EkanDatasetSerialiser extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id = '';

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label = '';


  /**
   * The file format for the serializer.
   *
   * @var string
   */
  public $format = '';

}

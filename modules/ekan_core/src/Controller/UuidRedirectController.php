<?php

namespace Drupal\ekan_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\ekan_core\Entity\EkanEntityBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for redirecting to an entity by uuid.
 */
class UuidRedirectController extends ControllerBase {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityRepository = $container->get('entity.repository');
    return $instance;
  }

  /**
   * Given an entity type ID and uuid, redirect to the entity view url.
   */
  public function redirectFromUuid($entity_type, $uuid) {
    $entity = $this->entityRepository->loadEntityByUuid($entity_type, $uuid);

    if ($entity instanceof EkanEntityBase && $entity->access('view')) {
      return new RedirectResponse($entity->toUrl()->toString());
    }

    return new RedirectResponse('/');
  }

}

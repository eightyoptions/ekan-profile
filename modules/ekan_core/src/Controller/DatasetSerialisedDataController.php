<?php

namespace Drupal\ekan_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to build Project Open Data files (data.json, catalog.xml).
 */
class DatasetSerialisedDataController extends ControllerBase {

  /**
   * Return the dataset dcat catalog.xml redirect.
   *
   * @param string $plugin_id
   *   E.g. EkanDatasetSerializer plugin id.
   *
   * @return \Drupal\Core\Routing\LocalRedirectResponse
   *   A redirect to the cached catalog.xml file.
   */
  public function retrieveAll($plugin_id): LocalRedirectResponse {

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.ekan_dataset_serialiser');
    $plugin = $manager->createInstance($plugin_id);
    if ($plugin instanceof EkanDatasetSerialiserInterface) {
      $file_path = $plugin->getCachedPath();
      $cachable_metadata = $plugin->cacheableMetadata();
      if (is_file($file_path)) {
        // Redirect should be cached for as long as the data is cached.
        $response = new LocalRedirectResponse(\Drupal::service('file_url_generator')->generateAbsoluteString($file_path));
        $response->getCacheableMetadata()->addCacheableDependency($cachable_metadata);
        $cache_expiry_time = \Drupal::time()->getRequestTime() + $cachable_metadata->getCacheMaxAge();
        $response->setExpires(\DateTime::createFromFormat('U', $cache_expiry_time));
        return $response;
      }
    }

    throw new NotFoundHttpException();
  }

  /**
   * Retrieve a single DCAT xml or json record for a dataset.
   *
   * @param \Drupal\ekan_core\Entity\EkanDatasetEntity $dataset
   *   The dataset entity.
   * @param string $plugin_id
   *   E.g. EkanDatasetSerializer plugin id.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The formatted response.
   */
  public function retrieveDataset(EkanDatasetEntity $dataset, $plugin_id): Response {

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.ekan_dataset_serialiser');
    $plugin = $manager->createInstance($plugin_id);
    if ($plugin instanceof EkanDatasetSerialiserInterface) {
      $build = $plugin->build([$dataset->id()]);
      $contents = $plugin->serialiseBuild($build);
      $mime_type = $plugin->getMimeType();
      return new Response($contents, 200, ['Content-Type' => $mime_type]);
    }

    throw new NotFoundHttpException();

  }

}

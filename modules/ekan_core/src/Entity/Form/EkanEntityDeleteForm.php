<?php

namespace Drupal\ekan_core\Entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a content_entity_example entity.
 *
 * @ingroup ekan_core
 */
class EkanEntityDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete entity %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl() {
    $resource = \Drupal::routeMatch()->getParameter('resource');
    $dataset = \Drupal::routeMatch()->getParameter('dataset');
    if ($resource) {
      return new Url('entity.resource.canonical', ['resource' => $resource->id()]);
    }
    elseif ($dataset) {
      return new Url('entity.dataset.canonical', ['dataset' => $dataset->id()]);
    }
    // Otherwise fallback to front.
    else {
      return new Url('<front>');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\ekan_core\Entity\EkanEntityBase $entity */
    $entity = $this->getEntity();
    $message = $this->getDeletionMessage();

    // Make sure that deleting a translation does not delete the whole entity.
    if (!$entity->isDefaultTranslation()) {
      $untranslated_entity = $entity->getUntranslated();
      $untranslated_entity->removeTranslation($entity->language()->getId());
      $untranslated_entity->save();
      $form_state->setRedirectUrl($untranslated_entity->toUrl('canonical'));
    }
    else {
      $entity->delete();
      $form_state->setRedirect('<front>');
    }

    // Set status message.
    $this->messenger()->addStatus($message);
    // Watch dog as well.
    $this->logger('ekan_core')->notice($message);
  }

  /**
   * {@inheritDoc}
   */
  protected function getDeletionMessage() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    if (!$entity->isDefaultTranslation()) {
      return $this->t('The content %label @language translation has been deleted.', [
        '%label' => $entity->label(),
        '@language' => $entity->language()->getName(),
      ]);
    }

    return $this->t('The content %label has been deleted.', ['%label' => $this->entity->label()]);
  }

}

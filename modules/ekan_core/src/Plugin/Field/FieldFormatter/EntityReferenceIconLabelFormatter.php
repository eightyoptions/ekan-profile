<?php

namespace Drupal\ekan_core\Plugin\Field\FieldFormatter;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_icon_label",
 *   label = @Translation("Label with Icon"),
 *   description = @Translation("Display the label of the referenced entities.
 *   Includes icon if available"), field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceIconLabelFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    if ($this->getSetting('link')) {
      foreach ($elements as $delta => &$element) {
        $item = $items->get($delta);
        $entity_id = $item->getValue()['target_id'] ?? NULL;
        if ($entity_id) {
          $element['#url'] = Url::fromUri('internal:/search', [
            'query' => ['f' => ["topic:$entity_id"]],
          ]);
        }
      }
    }

    foreach (Element::children($elements) as $delta) {
      $element = &$elements[$delta];

      $entity = NULL;

      if ($items instanceof EntityReferenceFieldItemListInterface) {
        $entity = $items->referencedEntities()[$delta] ?? NULL;
        // Load the referenced item in the current language if it has one.
        if ($entity instanceof TranslatableInterface && $entity->hasTranslation($langcode)) {
          $entity = $entity->getTranslation($langcode);
        }
      }

      if (!$entity) {
        continue;
      }

      /** @var \Drupal\ekan_core\Service\EkanIconHandler $icon_handler */
      $icon_handler = \Drupal::service('ekan_core.icon_handler');
      $icon = $icon_handler->getIconForEntity($entity);
      $label = $entity->label();
      $title_markup = new FormattableMarkup("$icon&nbsp;$label", []);

      if (isset($element['#title'])) {
        $element['#title'] = $title_markup;
      }
      else {
        unset($element['#plain_text']);
        $element['#markup'] = $title_markup;
      }
    }
    return $elements;
  }

}

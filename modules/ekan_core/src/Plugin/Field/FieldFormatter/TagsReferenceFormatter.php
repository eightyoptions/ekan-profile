<?php

namespace Drupal\ekan_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'tags reference formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "tags_reference_formatter",
 *   label = @Translation("Tags Formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TagsReferenceFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    if ($this->getSetting('link')) {
      foreach ($elements as $delta => &$element) {
        $item = $items->get($delta);
        $entity_id = $item->getValue()['target_id'] ?? NULL;
        if ($entity_id) {
          $element['#url'] = Url::fromUri('internal:/search', [
            'query' => ['f' => ["tags:$entity_id"]],
          ]);
        }
      }
    }

    $elements['#type'] = 'container';
    $elements['#attributes']['class'] = ['inline-tags'];

    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $handler_settings = $field_definition->getSetting('handler_settings');
    if (isset($handler_settings['target_bundles'])) {
      return in_array('tags', $handler_settings['target_bundles']);
    }
    return FALSE;
  }

}

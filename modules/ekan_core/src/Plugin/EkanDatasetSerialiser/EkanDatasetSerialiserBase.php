<?php

namespace Drupal\ekan_core\Plugin\EkanDatasetSerialiser;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class which EkanDatasetSerialiser plugins should extend from.
 */
abstract class EkanDatasetSerialiserBase extends PluginBase implements EkanDatasetSerialiserInterface, ContainerFactoryPluginInterface {
  /**
   * The file system used for reading and writing caches.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;


  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * The file url generator srvice.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;


  /**
   * The file format for the output e.g. xml or json.
   *
   * @var string
   */
  protected $format;


  /**
   * Logger for exception logging.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->format = $plugin_definition['format'];
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->fileSystem = $container->get('file_system');
    $instance->keyValueStore = $container->get('keyvalue')->get("dataset_serialiser:$plugin_id");
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    $instance->logger = $container->get('logger.factory')->get('ekan_core');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getUrlForFullCatalog(): Url {
    return $this->fileUrlGenerator->generate($this->getCachedPath());
  }

  /**
   * {@inheritDoc}
   */
  public function getFormat(): string {
    return $this->format;
  }

  /**
   * {@inheritDoc}
   */
  public function setFormat(string $format): void {
    $this->format = $format;
  }

  /**
   * {@inheritDoc}
   */
  public function getFilenamePrefix() {
    return 'catalog';
  }

  /**
   * Get path of individual dataset file.
   */
  public function getDatasetJsonFilePath($dataset_id) {
    $datasets_folder = $this->getDatasetsFolder();
    return "{$datasets_folder}/dataset_{$dataset_id}.json";
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheFolder() {
    return "public://serialised_datasets/{$this->getPluginId()}";
  }

  /**
   * Dataset files folder.
   */
  public function getDatasetsFolder() {
    return "{$this->getCacheFolder()}/datasets";
  }

  /**
   * {@inheritDoc}
   */
  public function getCachedPath() {
    $cache_folder = $this->getCacheFolder();
    return "{$cache_folder}/all_datasets.{$this->getFormat()}";
  }

  /**
   * {@inheritDoc}
   */
  public function isCacheStale() {
    if (!is_file($this->getCachedPath())) {
      return TRUE;
    }

    if ($this->keyValueStore->get('last_build') + $this->cacheableMetadata()->getCacheMaxAge() <= \Drupal::time()->getCurrentTime()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function warmCache() {
    $cached_filepath = $this->getCachedPath();
    $cache_folder = dirname($cached_filepath);
    $this->fileSystem->prepareDirectory($cache_folder, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $data = $this->serialiseBuild($this->build($this->getDatasetEntityIds()));

    if ($this->fileSystem->saveData($data, $cached_filepath, FileSystemInterface::EXISTS_REPLACE)) {
      $this->keyValueStore->set('last_build', \Drupal::time()->getCurrentTime());
      Cache::invalidateTags(["dataset_serialised:{$this->getPluginId()}"]);
      return $cached_filepath;
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateCache() {
    $this->keyValueStore->delete('last_build');
  }

  /**
   * {@inheritDoc}
   */
  public function getDatasetEntityIds(): array {
    $ids = \Drupal::entityQuery('dataset')
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();
    return $ids;
  }

  /**
   * {@inheritDoc}
   */
  public function buildDatasets(array $dataset_ids = []): array {

    $datasets = [];
    foreach ($dataset_ids as $dataset_id) {
      $dataset_filepath = $this->getDatasetJsonFilePath($dataset_id);

      if (!file_exists($dataset_filepath) || $this->isCacheStale()) {
        $dataset = EkanDatasetEntity::load($dataset_id);
        try {
          $this->createDatasetJsonFile($dataset);
        }
        catch (\Exception $ex) {
          Error::logException($this->logger, $ex);
        }
      }

      if (file_exists($dataset_filepath)) {
        $datasets[] = json_decode(file_get_contents($dataset_filepath), TRUE);
      }
    }

    return $datasets;
  }

  /**
   * Creates a dataset file. This gets created on every dataset entity save.
   */
  public function createDatasetJsonFile(EkanDatasetEntity $dataset) {
    $dataset_path = $this->getDatasetJsonFilePath($dataset->id());
    $dataset_folder = dirname($dataset_path);
    $directory_writable = $this->fileSystem->prepareDirectory($dataset_folder, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    if ($directory_writable) {
      $dataset_build = $this->buildDataset($dataset);
      $context = [
        'serialiser_plugin_id' => $this->getPluginId(),
        'entity_type' => 'dataset',
        'dataset' => $dataset,
      ];
      \Drupal::moduleHandler()->alter('ekan_dataset_serialiser_build', $dataset_build, $context);
      if (file_exists($dataset_path)) {
        $this->fileSystem->delete($dataset_path);
      }
      return $this->fileSystem->saveData(Json::encode($dataset_build), $dataset_path, FileSystemInterface::EXISTS_REPLACE);
    }
    else {
      throw new \Exception("Directory $dataset_path is not writable");
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildDistributions(EkanDatasetEntity $dataset) {
    $resources = [];
    foreach (\Drupal::entityTypeManager()->getStorage('resource')->loadByProperties(['dataset_ref' => $dataset->id()]) as $resource) {
      assert($resource instanceof EkanResourceEntity);
      $resource_build = $this->buildResource($resource);
      $context = [
        'serialiser_plugin_id' => $this->getPluginId(),
        'entity_type' => 'resource',
        'resource' => $resource,
        'dataset' => $dataset,
      ];
      \Drupal::moduleHandler()->alter('ekan_dataset_serialiser_build', $resource_build, $context);
      $resources[] = $resource_build;
    }
    return $resources;
  }

  /**
   * {@inheritDoc}
   */
  public function cacheableMetadata() {
    $cachable_metadata = new CacheableMetadata();
    $cachable_metadata->setCacheTags([
      'dataset_list',
      'resource_list',
      "dataset_serialised:{$this->getPluginId()}",
    ]);
    $cachable_metadata->setCacheMaxAge(24 * 60 * 60);
    return $cachable_metadata;
  }

  /**
   * Helper to filter empty values from datasets.
   */
  protected function filterEmptyValues($data) {

    foreach ($data as $key => &$value) {
      if (is_array($value)) {
        $value = $this->filterEmptyValues($value);
      }

      if (empty($value)) {
        unset($data[$key]);
      }
    }

    return $data;
  }

}

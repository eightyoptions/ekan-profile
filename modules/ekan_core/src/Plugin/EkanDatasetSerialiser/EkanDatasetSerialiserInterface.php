<?php

namespace Drupal\ekan_core\Plugin\EkanDatasetSerialiser;

use Drupal\Core\Url;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * Interface for serialising datasets.
 *
 * To be implemented by all plugins of type EkanDatasetSerialiser.
 */
interface EkanDatasetSerialiserInterface {

  /**
   * Return the api url for a given dataset, or null if not supported.
   */
  public function getUrlForDataset(EkanDatasetEntity $dataset): ?Url;

  /**
   * Return the url for the full catalog.
   */
  public function getUrlForFullCatalog(): ?Url;

  /**
   * Get the location of the cached data.
   *
   * Get the cached generated file path or null if something went wrong.
   *
   * @return null|string
   *   The file path to the data file.
   */
  public function getCachedPath();

  /**
   * Check if the cache is stale.
   *
   * @return bool
   *   TRUE if the cache is stale.
   */
  public function isCacheStale();

  /**
   * Invalidate caches for this build.
   */
  public function invalidateCache();

  /**
   * Warm the cached data.
   *
   * @return string
   *   The filename of the generated file.
   */
  public function warmCache();

  /**
   * The filename prefix for the cached file.
   *
   * @return string
   *   e.g. data_.
   */
  public function getFilenamePrefix();

  /**
   * The folder to store the cached data.
   *
   * @return string
   *   e.g. public://my_cache.
   */
  public function getCacheFolder();

  /**
   * Build the structured data before returning.
   *
   * @return array
   *   The structured data before serialization.
   */
  public function build(array $dataset_ids): array;

  /**
   * How the structured data of all datasets is serialised.
   */
  public function serialiseBuild(array $build): string;

  /**
   * Return a list of ekan dataset entity ids.
   */
  public function getDatasetEntityIds(): array;

  /**
   * Build the full data structure ready for serialization.
   *
   * @return array
   *   The array of all the datasets.
   */
  public function buildDatasets(array $dataset_ids = []): array;

  /**
   * Build the distributions section of the dataset item.
   *
   * @param \Drupal\ekan_core\Entity\EkanDatasetEntity $dataset
   *   The dataset to base this list on.
   *
   * @return array
   *   The array of resources within the dataset.
   */
  public function buildDistributions(EkanDatasetEntity $dataset);

  /**
   * Sets up cachable metadata for this controller.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   Cacheable metadata which applies to assets in this controller.
   */
  public function cacheableMetadata();

  /**
   * Return a mime type for the given format.
   *
   * E.g. xml might return application/rdf+xml.
   *
   * @return string
   *   The mime type string e.g. application/json
   */
  public function getMimeType(): string;

  /**
   * Set the output format for the instance of this builder.
   *
   * @param string $format
   *   The format e.g. xml or json.
   */
  public function setFormat(string $format): void;

  /**
   * The output format for this instance.
   *
   * @return string
   *   e.f. xml or json.
   */
  public function getFormat(): string;

  /**
   * Build data structure for a resource entity.
   */
  public function buildResource(EkanResourceEntity $resource): array;

  /**
   * Build data structure for a dataset entity.
   */
  public function buildDataset(EkanDatasetEntity $dataset): array;

}

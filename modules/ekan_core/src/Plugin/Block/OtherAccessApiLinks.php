<?php

namespace Drupal\ekan_core\Plugin\Block;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Plugin\EkanDatasetSerialiser\EkanDatasetSerialiserInterface;

/**
 * Provides a 'other_access_api_links' block.
 *
 * This is a port of the API Access block from DKAN.
 *
 * @Block(
 *  id = "other_access_api_links",
 *  admin_label = @Translation("Other Access API Links"),
 *  context_definitions = {
 *    "dataset" = @ContextDefinition(
 *       "entity:dataset",
 *       label = @Translation("Dataset"),
 *       required = TRUE
 *     )
 *  }
 * )
 */
class OtherAccessApiLinks extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function build(): array {

    $build = [];

    $dataset = $this->getContextValue('dataset');

    if (!($dataset instanceof EkanDatasetEntity)) {
      return $build;
    }

    CacheableMetadata::createFromObject($dataset)->applyTo($build);

    $build['content'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'api-links'],
      'formats' => [
        '#type' => 'container',
        '#markup' => t('The information on this page (the dataset metadata) is also available in these formats.'),
      ],
      'buttons' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['btn-group']],
      ],
    ];

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.ekan_dataset_serialiser');
    foreach ($manager->getDefinitions() as $plugin_id => $definition) {
      $instance = $manager->createInstance($plugin_id);
      assert($instance instanceof EkanDatasetSerialiserInterface);
      assert($instance instanceof PluginBase);
      if ($instance->isCacheStale()) {
        $filepath = $instance->warmCache();
        \Drupal::logger('ekan_core')->info("Generated $filepath");
      }

      $url = $instance->getUrlForDataset($dataset);
      if (!$url) {
        continue;
      }

      try {
        $build['content']['buttons'][$plugin_id] = [
          '#type' => 'link',
          '#title' => [
            '#type' => 'inline_template',
            '#template' => "<i class='ckan-icon ckan-icon-{{ format }}'></i> {{ label }}",
            '#context' => [
              'format' => $instance->getFormat(),
              'label' => $instance->getPluginDefinition()['label'],
            ],
          ],
          '#url' => $url,
          '#attributes' => [
            'class' => ['btn', 'btn-outline-secondary', 'btn-sm'],
          ],
        ];

      }
      catch (\Exception $ex) {
        return $build;
      }
    }

    return $build;
  }

}

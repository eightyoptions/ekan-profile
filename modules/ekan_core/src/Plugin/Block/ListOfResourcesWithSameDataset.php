<?php

namespace Drupal\ekan_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\ekan_core\Entity\EkanDatasetEntity;

/**
 * Provides a 'resources_with_same_dataset' block.
 *
 * @Block(
 *  id = "resources_with_same_dataset",
 *  admin_label = @Translation("Related Resources"),
 * )
 */
class ListOfResourcesWithSameDataset extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $route_name = \Drupal::routeMatch()->getRouteName();
    if (!$route_name == 'entity.resource.view') {
      return [];
    }
    $build = [];
    $build['related_resources'] = $this->buildLinks();

    return $build;
  }

  /**
   * Helper to build workflow links.
   */
  protected function buildLinks() {

    $cache_metadata = new CacheableMetadata();

    $list = [];
    // EkanResourceEntity $resource.
    $resource = \Drupal::routeMatch()->getParameter('resource');
    if ($resource) {
      $cache_metadata->addCacheableDependency($resource);
      $dataset_id = $resource->get('dataset_ref')->target_id;

      if ($dataset_id) {
        $dataset = EkanDatasetEntity::load($dataset_id);
        $cache_metadata->addCacheableDependency($dataset);

        // Get all the related resources id.
        $all_related_resources = \Drupal::entityQuery('resource')
          ->accessCheck(TRUE)
          ->condition('status', 1)
          ->condition('dataset_ref', $dataset_id)
          ->execute();

        foreach ($all_related_resources as $related_resource_id) {
          $classes = ['list_of_resources'];

          // Get the resource title from the id.
          /** @var \Drupal\Core\Entity\EntityRepositoryInterface $er */
          $er = \Drupal::service('entity.repository');
          $related_resource = $er->getActive('resource', $related_resource_id);
          $label = $related_resource->label();

          if ($resource->id() == $related_resource_id) {
            $classes[] = 'active';
          }

          $list[] = $related_resource->toLink($label, 'canonical', ['attributes' => ['class' => $classes]])->toRenderable();
        }
        $cache_metadata->applyTo($list);
      }
    }
    return $list;
  }

}

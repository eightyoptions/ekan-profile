<?php

/**
 * @file
 * Ekan_core.api.php file.
 */

/**
 * Alter EKAN license options.
 *
 * @param array $options
 *   License options.
 */
function hook_ekan_license_options_alter(array &$options) {
  $options = [
    "my-custom-license-1" => [
      "label" => t("My First Custom License"),
      "uri" => "https://example.org/licenses/by/4.0/",
    ],
    "my-custom-license-2" => [
      "label" => t("My Second Custom License"),
      "uri" => "https://example.org/licenses/by/5.0/",
    ],
  ];
}

/**
 * Alter dataset and resource prior to serialisation.
 *
 * This will allow customisations to /data.json and /catalog.xml
 * endpoints.
 */
function hook_ekan_dataset_serialiser_build_alter(&$build, $context) {

  if ($context['serialiser_plugin_id'] == 'dcat_ap_2_0_xml') {

    if ($context['entity_type'] == 'resource') {
      /** @var \Drupal\ekan_core\Entity\EkanDatasetEntity $dataset */
      $dataset = $context['dataset'];

      /** @var \Drupal\ekan_core\Entity\EkanResourceEntity $resource */
      $resource = $context['resource'];
      $build['dct:description'] = $resource->get('field_my_custom_description')->value;
    }

    if ($context['entity_type'] == 'dataset') {
      /** @var \Drupal\ekan_core\Entity\EkanDatasetEntity $dataset */
      $dataset = $context['dataset'];

      $build['dcat:contactPoint']['vcard:Kind']['vcard:fn'] = $dataset->get('field_my_custom_contact')->value;
      $build['dcat:keyword'][] = 'New custom keyword';
    }
  }
}

/**
 * Alter the configuration of a chart block before build.
 *
 * @param array $configuration
 *   The block configurations.
 * @param array $context
 *   Contextual data e.g: plugin_id.
 */
function hook_ekan_statistics_chart_pre_build_alter(array &$configuration, array $context) {
  $plugin_id = $context['plugin_id'];
  if ($plugin_id == 'some_block_plugin_id') {
    // Fetch 10 rows.
    $configuration['limit'] = 10;
    // Sort desc or asc.
    $configuration['sort'] = 'desc';
  }
}

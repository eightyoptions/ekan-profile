<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * A Data table visualisation.
 *
 * @EkanVisualisation(
 *   id = "stacked_line_chart",
 *   label = @Translation("Stacked Line Chart"),
 *   formats = {
 *   "csv"
 *   }
 * )
 */
class StackedLineChart extends EkanVisualisationBase implements CsvVisualiserInterface {

  use CsvVisualiserTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'x_axis_column' => '1',
      'series_columns' => '2',
      'series_column_labels' => '',
      'has_header' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['x_axis_column'] = [
      '#title' => t('x-axis column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['x_axis_column'],
    ];

    $elements['series_columns'] = [
      '#title' => t('Series Columns'),
      '#type' => 'textfield',
      '#description' => t('Comma separated list of column names or numbers which hold the series data. where first column is 1.'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['series_columns'],
    ];
    $elements['series_column_labels'] = [
      '#title' => t('Series Labels (if different to table header labels)'),
      '#type' => 'textfield',
      '#description' => t('Comma separated list of series labels which align with the series column above'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['series_column_labels'],
    ];

    $elements['has_header'] = [
      '#title' => t('CSV file has header row'),
      '#type' => 'checkbox',
      '#description' => t("Tick if first row is header, so it won't be included in the chart."),
      '#default_value' => $this->configuration['has_header'],
      '#required' => FALSE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {
    $elements = [];

    $id = $this->getUniqueId($resource);

    $series_columns = array_filter(explode(',', $this->configuration['series_columns']));
    $series_columns = array_map('trim', $series_columns);
    foreach ($series_columns as &$column) {
      $column = $this->convertColumnKey($column);
    }

    $series_labels = [];
    if (!empty(trim($this->configuration['series_column_labels']))) {
      $series_labels = array_filter(explode(',', $this->configuration['series_column_labels']));
      $series_labels = array_map('trim', $series_labels);
    }
    else {
      foreach ($series_columns as $column_key) {
        $series_labels[] = $this->getCsvHeaderValue($resource, $column_key);
      }
    }

    $options = [
      // primary, secondary colours.
      'color' => ['#145b98', '#002245'],
    ];

    $options['tooltip']['trigger'] = 'axis';
    $options['legend']['data'] = $series_labels ?? $series_columns;

    foreach ($series_columns as $i => $column_key) {
      $options['series'][] = [
        "name" => $series_labels[$i] ?? $column_key,
        "type" => 'line',
        "data" => $this->getCsvData($resource, $column_key),
      ];
    }

    $x_axis_column = $this->convertColumnKey($this->configuration['x_axis_column']);

    $x_axis_labels = $this->getCsvData($resource, $x_axis_column);

    $options['xAxis'] = [
      'type' => 'category',
      'data' => $x_axis_labels,
      'axisLabel' => [
        'rotate' => 45,
      ],
    ];

    $options['yAxis']['type'] = 'value';

    $elements[$id] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$id],
        'id' => [$id],
        'style' => 'height: 650px;',
      ],
      '#attached' => [
        'library' => ['ekan_core/generic-echart'],
        'drupalSettings' => [
          'generic_echart' => [
            $id => $options,
          ],
        ],
      ],
    ];

    return $elements;
  }

}

<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Utility\Error;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\file\FileInterface;
use League\Csv\Reader;
use League\Csv\UnavailableStream;

/**
 * Trait used by various different CSV visualisation plugins.
 */
trait CsvVisualiserTrait {

  /**
   * {@inheritDoc}
   */
  public function hasHeader(): bool {
    return $this->configuration['has_header'];
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvHeaderValue(EkanResourceEntity $resource, $column_data_key) {
    $reader = $this->getCsvReader($resource);
    if (!$reader) {
      return $column_data_key;
    }

    $row = $reader->fetchOne();
    if (!is_numeric($column_data_key)) {
      $row = array_combine($row, $row);
    }

    return $row[$column_data_key] ?? $column_data_key;
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvReader(EkanResourceEntity $resource): ?Reader {
    $file = $this->getCsvFile($resource);
    if (!$file) {
      return NULL;
    }

    try {
      return Reader::createFromPath($file->getFileUri());
    }
    catch (UnavailableStream $ex) {
      Error::logException($this->logger, $ex);
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvData(EkanResourceEntity $resource, $column_data_key = NULL): array {

    $reader = $this->getCsvReader($resource);
    if (!$reader) {
      return [];
    }

    if ($this->hasHeader()) {
      $reader->setHeaderOffset(0);
    }

    if (!is_null($column_data_key)) {
      $results = $reader->fetchColumn($column_data_key);
    }
    else {
      $results = $reader->getRecords();
    }

    return array_values(iterator_to_array($results));
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvFile(EkanResourceEntity $resource): ?FileInterface {
    if (!$resource->get('upload')->getEntity()) {
      return NULL;
    }

    /** @var \Drupal\taxonomy\TermInterface $format */
    $format = $resource->get('resource_format')->entity;
    if ($format->getName() == 'csv') {
      /** @var \Drupal\file\Entity\File $file */
      $file = $resource->get('upload')->entity;
      if ($file instanceof FileInterface && $file->getMimeType() == 'text/csv') {
        return $file;
      }
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function convertColumnKey($value): mixed {

    // If it's numeric, return a non-negative integer.
    if (is_numeric($value)) {
      return max((int) $value - 1, 0);
    }

    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function getColumnValue(array $row, mixed $key): ?string {
    $integer_keyed = array_values($row);
    if (is_numeric($key)) {
      return $integer_keyed[$key] ?? NULL;
    }
    else {
      return $row[$key] ?? NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicable(EkanResourceEntity $resource): bool {
    return (bool) $this->getCsvFile($resource) && parent::isApplicable($resource);
  }

}

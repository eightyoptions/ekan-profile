<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\file\FileInterface;
use League\Csv\Reader;

/**
 * For visualisers which deal with CSV resources.
 */
interface CsvVisualiserInterface {

  /**
   * Is there a header row in the CSV?
   */
  public function hasHeader(): bool;

  /**
   * Get CSV data from the given resource.
   *
   * If you pass in a column key, an array of data just for that
   * column is returned.
   */
  public function getCsvData(EkanResourceEntity $resource, $column_data_key = NULL): array;

  /**
   * Fetch the CSV file from the resource.
   */
  public function getCsvFile(EkanResourceEntity $resource): ?FileInterface;

  /**
   * Get a single header values for a CSV.
   */
  public function getCsvHeaderValue(EkanResourceEntity $resource, $column_data_key);

  /**
   * Returns the CSV reader for ths CSV of a resource.
   */
  public function getCsvReader(EkanResourceEntity $resource): ?Reader;

  /**
   * Fetch a column value from a row of data.
   *
   * Supports numeric or string based key.
   */
  public function getColumnValue(array $row, mixed $key): ?string;

  /**
   * If a column key is configured by the user, convert it.
   *
   * For use by the CSV reader.
   */
  public function convertColumnKey($value): mixed;

}

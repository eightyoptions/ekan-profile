<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * Interface for Ekan Visualisation plugins.
 */
interface EkanVisualisationInterface extends ConfigurableInterface {

  /**
   * The settings form for Ekan Visualisation.
   */
  public function settingsForm(array $form, FormStateInterface $form_state);

  /**
   * Creates the renderable visualisation of the resource.
   *
   * It uses the plugin configuration to create a renderable array
   * or null if there is no file or conditions are not met.
   */
  public function visualise(EkanResourceEntity $resource): ?array;

  /**
   * Checks the access for visualisation plugin.
   */
  public function visualisationAccess(AccountInterface $account);

  /**
   * Get the unique ID for this resource visualisation.
   *
   * So that a resource can be visualised multiple times on
   * a page.
   */
  public function getUniqueId(EkanResourceEntity $resource): string;

  /**
   * Check if this visualisation plugin applies to a resource.
   */
  public function isApplicable(EkanResourceEntity $resource): bool;

}

<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * A Data table visualisation.
 *
 * @EkanVisualisation(
 *   id = "basic_pie_chart",
 *   label = @Translation("Basic Pie Chart"),
 *   formats = {
 *   "csv"
 *   }
 * )
 */
class BasicPieChart extends EkanVisualisationBase implements CsvVisualiserInterface {

  use CsvVisualiserTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'labels_column' => '1',
      'values_column' => '2',
      'has_header' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['labels_column'] = [
      '#title' => t('Labels column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['labels_column'],
    ];
    $elements['values_column'] = [
      '#title' => t('Values column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['values_column'],
    ];
    $elements['has_header'] = [
      '#title' => t('CSV file has header row'),
      '#type' => 'checkbox',
      '#description' => t("Tick if first row is header, so it won't be included in the chart."),
      '#default_value' => $this->configuration['has_header'],
      '#required' => FALSE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {
    $elements = [];
    $id = $this->getUniqueId($resource);

    $values_column = $this->convertColumnKey($this->configuration['values_column']);
    $labels_column = $this->convertColumnKey($this->configuration['labels_column']);

    $data = [];
    foreach ($this->getCsvData($resource) as $row) {

      $value = $this->getColumnValue($row, $values_column);
      $name = $this->getColumnValue($row, $labels_column);

      $data[] = [
        'value' => $value,
        'name' => $name,
      ];
    }

    $elements[$id] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$id],
        'id' => [$id],
        'style' => 'height: 450px;',
      ],
      '#attached' => [
        'library' => ['ekan_visuals/basic-pie-chart'],
        'drupalSettings' => [
          'pie_chart' => [
            $id => [
              'data' => $data,
              'id' => $id,
            ],
          ],
        ],
      ],
    ];

    return $elements;
  }

}

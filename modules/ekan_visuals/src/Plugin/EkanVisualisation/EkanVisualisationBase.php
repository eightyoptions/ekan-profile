<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Ekan Visualisation plugins.
 */
abstract class EkanVisualisationBase extends PluginBase implements EkanVisualisationInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Log channel for exception logging.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('ekan_visuals');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function visualisationAccess(AccountInterface $account) {
    // By default, allow access.
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getUniqueId(EkanResourceEntity $resource): string {
    return Html::getUniqueId("{$this->getPluginId()}-{$resource->id()}");
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EkanResourceEntity $resource): bool {
    if (!$resource->get('resource_format')->entity) {
      return FALSE;
    }

    $format = $resource->get('resource_format')->entity;
    if ($format instanceof TermInterface) {
      if (empty($this->getPluginDefinition()['formats'])) {
        return TRUE;
      }

      foreach ($this->getPluginDefinition()['formats'] as $allowed_format) {
        $allowed_format = preg_quote($allowed_format);
        if (preg_match("/^$allowed_format$/i", $format->label())) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

}

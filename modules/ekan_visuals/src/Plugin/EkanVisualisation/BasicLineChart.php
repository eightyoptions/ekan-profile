<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * A Data table visualisation.
 *
 * @EkanVisualisation(
 *   id = "basic_line_chart",
 *   label = @Translation("Basic Line Chart"),
 *   formats = {
 *   "csv"
 *   }
 * )
 */
class BasicLineChart extends BasicBarChart implements CsvVisualiserInterface {

  use CsvVisualiserTrait;

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {

    $elements = [];

    $id = $this->getUniqueId($resource);

    $x_axis_column = $this->convertColumnKey($this->configuration['x_axis_column']);
    $y_axis_column = $this->convertColumnKey($this->configuration['y_axis_column']);

    $elements[$id] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$id],
        'id' => [$id],
        'style' => 'height: 650px;',
      ],
      '#attached' => [
        'library' => ['ekan_visuals/basic-line-chart'],
        'drupalSettings' => [
          'line_chart' => [
            $id => [
              'x_axis_values' => $this->getCsvData($resource, $x_axis_column),
              'x_axis_label' => $this->configuration['x_axis_label'],
              'y_axis_values' => $this->getCsvData($resource, $y_axis_column),
              'y_axis_label' => $this->configuration['y_axis_label'],
              'id' => $id,
            ],
          ],
        ],
      ],
    ];

    return $elements;
  }

}

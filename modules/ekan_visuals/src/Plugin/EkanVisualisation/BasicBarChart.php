<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * A Data table visualisation.
 *
 * @EkanVisualisation(
 *   id = "basic_bar_chart",
 *   label = @Translation("Basic Bar Chart"),
 *   formats = {
 *   "csv"
 *   }
 * )
 */
class BasicBarChart extends EkanVisualisationBase implements CsvVisualiserInterface {

  use CsvVisualiserTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'x_axis_column' => '1',
      'x_axis_label' => '',
      'y_axis_column' => '2',
      'y_axis_label' => '',
      'has_header' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['x_axis_column'] = [
      '#title' => t('x-axis column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['x_axis_column'],
    ];
    $elements['x_axis_label'] = [
      '#title' => t('x-axis Label'),
      '#type' => 'textfield',
      '#description' => t('Label for x-axis label.'),
      '#default_value' => $this->configuration['x_axis_label'],
    ];
    $elements['y_axis_column'] = [
      '#title' => t('y-axis column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['y_axis_column'],
    ];
    $elements['y_axis_label'] = [
      '#title' => t('y-axis Label'),
      '#type' => 'textfield',
      '#description' => t('Label for y-axis label.'),
      '#default_value' => $this->configuration['y_axis_label'],
    ];
    $elements['has_header'] = [
      '#title' => t('CSV file has header row'),
      '#type' => 'checkbox',
      '#description' => t("Tick if first row is header, so it won't be included in the bar chart."),
      '#default_value' => $this->configuration['has_header'],
      '#required' => FALSE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {

    $elements = [];

    $x_axis_column = $this->convertColumnKey($this->configuration['x_axis_column']);
    $y_axis_column = $this->convertColumnKey($this->configuration['y_axis_column']);

    $id = $this->getUniqueId($resource);
    $elements[$id] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$id],
        'id' => [$id],
        'style' => 'height: 650px;',
      ],
      '#attached' => [
        'library' => ['ekan_visuals/basic-bar-chart'],
        'drupalSettings' => [
          'bar_chart' => [
            $id => [
              'x_axis_values' => $this->getCsvData($resource, $x_axis_column),
              'x_axis_label' => $this->configuration['x_axis_label'],
              'y_axis_values' => $this->getCsvData($resource, $y_axis_column),
              'y_axis_label' => $this->configuration['y_axis_label'],
              'id' => $id,
            ],
          ],
        ],
      ],
    ];

    return $elements;
  }

}

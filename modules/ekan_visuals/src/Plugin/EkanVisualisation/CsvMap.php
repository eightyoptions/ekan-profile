<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\leaflet\LeafletSettingsElementsTrait;

/**
 * A CSV map visualisation based on latitude and longitude columns from CSV.
 *
 * @EkanVisualisation(
 *   id = "csv_map",
 *   label = @Translation("CSV Map"),
 *   formats = {
 *   "csv"
 *   }
 * )
 */
class CsvMap extends EkanVisualisationBase implements CsvVisualiserInterface {

  use CsvVisualiserTrait;
  use LeafletSettingsElementsTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'lat_column' => '',
      'long_column' => '',
      'has_header' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['lat_column'] = [
      '#title' => t('Latitude column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#default_value' => $this->configuration['lat_column'],
    ];

    $elements['long_column'] = [
      '#title' => t('Longitude column'),
      '#type' => 'textfield',
      '#description' => t('Enter the column name or numeric column number, where first column is 1.'),
      '#default_value' => $this->configuration['long_column'],
    ];
    $elements['has_header'] = [
      '#title' => t('CSV file has header row'),
      '#type' => 'checkbox',
      '#description' => t("Tick if first row is header."),
      '#default_value' => $this->configuration['has_header'],
      '#required' => FALSE,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {
    /** @var \Drupal\leaflet\LeafletService $leaflet_service */
    $leaflet_service = \Drupal::service('leaflet.service');

    $formatted_data = $this->readCsv($resource);

    $height = '400px';
    $id = $this->getUniqueId($resource);
    $elements = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['visualisation-csv-leaflet-map'],
        'style' => "height: $height;",
      ],
    ];
    $map = leaflet_map_get_info('OSM Mapnik');
    $map['id'] = $id;
    $map['geofield_cardinality'] = 1;
    $this->setAdditionalMapOptions($map, [
      'map_lazy_load' => [
        'lazy_load' => 1,
      ],
    ]);
    $features[] = [
      'type' => 'point',
      'group' => $formatted_data,
      'features' => $formatted_data,
      'entity_id' => $resource->id(),
    ];
    $elements[$id] = $leaflet_service->leafletRenderMap($map, $features, $height);

    return $elements;
  }

  /**
   * Helper function to read csv and format data for bar chart.
   */
  public function readCsv(EkanResourceEntity $resource) {

    $lat_column = $this->convertColumnKey($this->configuration['lat_column']);
    $long_column = $this->convertColumnKey($this->configuration['long_column']);

    $formatted_data = [];

    $headers = NULL;
    if ($this->hasHeader()) {
      $headers = $this->getCsvReader($resource)->fetchOne();
    }

    foreach ($this->getCsvData($resource) as $row) {

      $lat = $this->getColumnValue($row, $lat_column);
      $lon = $this->getColumnValue($row, $long_column);

      if (!is_numeric($lat) || !is_numeric($lon)) {
        continue;
      }

      $data = [
        'type' => 'point',
        'lat' => $lat,
        'lon' => $lon,
      ];

      $popup_data = [];
      if ($headers) {
        foreach ($headers as $header) {
          $popup_data[] = "<strong>$header:</strong> $row[$header]";
        }
      }
      else {
        foreach ($row as $index => $col_value) {
          $row_num = $index + 1;
          $popup_data[] = t("<strong>Value %row_num:</strong> %col_value", [
            '%row_num' => $row_num,
            '%col_value' => $col_value,
          ]);
        }
      }

      $data['popup']['value'] = implode("<br>", $popup_data);

      $formatted_data[] = $data;
    }

    return $formatted_data;
  }

}

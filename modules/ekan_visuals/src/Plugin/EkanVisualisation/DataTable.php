<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use League\Csv\InvalidArgument;
use League\Csv\Reader;

/**
 * A Data table visualisation.
 *
 * @EkanVisualisation(
 *   id = "data_table",
 *   label = @Translation("Data Table"),
 *   formats = {
 *   "csv"
 *   }
 * )
 */
class DataTable extends EkanVisualisationBase implements CsvVisualiserInterface {

  use CsvVisualiserTrait {
    CsvVisualiserTrait::getCsvReader as protected getCsvReaderFromTrait;
  }

  const MAX_ROWS = 1000;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'csvfile_number_of_rows' => 50,
      'csvfile_formatter_has_header' => TRUE,
      'csvfile_formatter_separator' => ',',
      'csvfile_formatter_enclosure' => '"',
      'csvfile_formatter_escape' => '\\',
      'csvfile_formatter_smart_urls' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['csvfile_number_of_rows'] = [
      '#title' => t('Number of rows to display'),
      '#type' => 'number',
      '#description' => t('Leave empty to display the maximum number of rows, currently set to %max_rows rows.', ['%max_rows' => self::MAX_ROWS]),
      '#min' => 1,
      '#max' => self::MAX_ROWS,
      '#default_value' => $this->configuration['csvfile_number_of_rows'],
    ];

    $elements['csvfile_formatter_has_header'] = [
      '#title' => t('CSV file has header row'),
      '#type' => 'checkbox',
      '#description' => t('Enable if the first row of the CSV file is a header row.'),
      '#default_value' => $this->configuration['csvfile_formatter_has_header'],
      '#required' => FALSE,
    ];

    $elements['csvfile_formatter_separator'] = [
      '#title' => t('CSV field separator'),
      '#description' => t('Character separating fields in a CSV row.'),
      '#type' => 'textfield',
      '#size' => 1,
      '#default_value' => $this->configuration['csvfile_formatter_separator'],
      '#required' => TRUE,
    ];

    $elements['csvfile_formatter_enclosure'] = [
      '#title' => t('CSV field enclosure character'),
      '#description' => t('Character indicating an enclosed field in a CSV row.'),
      '#type' => 'textfield',
      '#size' => 1,
      '#default_value' => $this->configuration['csvfile_formatter_enclosure'],
      '#required' => FALSE,
    ];

    $elements['csvfile_formatter_escape'] = [
      '#title' => t('CSV field escape character'),
      '#description' => t('Character indicating an escape sequence in a CSV row.'),
      '#type' => 'textfield',
      '#size' => 1,
      '#default_value' => $this->configuration['csvfile_formatter_escape'],
      '#required' => FALSE,
    ];

    // @todo do we want this feature enabled or totally removed?
    $elements['csvfile_formatter_smart_urls'] = [
      '#title' => t('CSV smart URLs'),
      '#type' => 'hidden',
      '#description' => t('Find URLs and render them into links.'),
      '#default_value' => $this->configuration['csvfile_formatter_smart_urls'],
      '#required' => FALSE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function hasHeader(): bool {
    return $this->configuration['csvfile_formatter_has_header'] ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvReader4(EkanResourceEntity $resource): ?Reader {
    $reader = $this->getCsvReaderFromTrait($resource);
    if (!$reader) {
      return NULL;
    }

    try {
      if (isset($this->configuration['csvfile_formatter_enclosure'])) {
        $reader->setEnclosure($this->configuration['csvfile_formatter_enclosure']);
      }

      if (isset($this->configuration['csvfile_formatter_escape'])) {
        $reader->setEscape($this->configuration['csvfile_formatter_escape']);
      }

      if (isset($this->configuration['csvfile_formatter_separator'])) {
        $reader->setDelimiter($this->configuration['csvfile_formatter_separator']);
      }
    }
    catch (InvalidArgument $ex) {
      Error::logException($this->logger, $ex);
    }

    return $reader;
  }

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {

    // Create data rows.
    $reader = $this->getCsvReader($resource);
    if (!$reader) {
      return ['#markup' => $this->t('There was a problem with the visualisation plugin')];
    }

    $table_header = [];
    $table_rows = [];

    $offset = 0;
    if ($this->hasHeader()) {
      $table_header = $reader->fetchOne(0);
      $reader->setHeaderOffset(0);
    }

    $max_rows = $this->configuration['csvfile_number_of_rows'] ?: self::MAX_ROWS;
    while ($offset < ($max_rows)) {
      try {
        $record = $reader->fetchOne($offset++);
      }
      catch (\Exception $ex) {
        \Drupal::messenger()->addError($ex->getMessage());
        break;
      }

      $row = [];
      foreach ($record as $column) {
        $row['data'][] = $this->processColumnData($column, $this->configuration);
      }
      $table_rows[] = $row;
    }

    // Is there any more records to read? It helps to figure out if we need
    // to add a help message later.
    $more_records = FALSE;
    try {
      $record = $reader->fetchOne($offset++);
      if (!empty($record)) {
        $more_records = TRUE;
      }
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addError($ex->getMessage());
    }

    $id = $this->getUniqueId($resource);

    $elements[$id] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['visualisation table'],
        'style' => "max-width: 100rem; overflow-x:auto; max-height: 100rem;",
      ],
      'description' => [],
      'table' => [
        '#sticky' => TRUE,
        '#theme' => 'table',
        '#rows' => $table_rows,
        '#header' => $table_header,
        '#attributes' => ['class' => ['table-responsive']],
      ],
    ];

    if ($more_records) {
      // Add help message to explain that there are more records to show.
      $elements[$id]['description'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['alert alert-warning'],
        ],
        '#markup' =>
        $this->t('Previewing %max_rows records. If you prefer to preview all available data for this resource, please download the CSV resource.',
            ['%max_rows' => $max_rows]),
      ];
    }

    return $elements;
  }

  /**
   * Process column items.
   */
  private function processColumnData($column_data, $settings) {
    $result = $column_data;

    if ($settings['csvfile_formatter_smart_urls']) {
      // Make links out of URLs in the CSV files.
      $matched = filter_var($column_data, FILTER_VALIDATE_URL);
      if ($matched !== FALSE) {
        $url = Url::fromUri($matched);
        $result = Link::fromTextAndUrl($matched, $url)->toString();
      }
      // Make links out of email addresses in the CSV files.
      $matched = filter_var($column_data, FILTER_VALIDATE_EMAIL);
      if ($matched !== FALSE) {
        $url = Url::fromUri('mailto:' . $matched);
        $result = Link::fromTextAndUrl($matched, $url)->toString();
      }
      // Bonus: Make links out of simple Markdown link syntax ([text](link))
      // in the CSV files.
      $matched = preg_match('/\[(.*)\]\((.*)\)/', $column_data, $matches);
      if ($matched != 0) {
        $url = Url::fromUri($matches[2]);
        $result = Link::fromTextAndUrl($matches[1], $url)->toString();
      }
    }

    return $result;
  }

}

<?php

namespace Drupal\ekan_visuals\Plugin\EkanVisualisation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * ArcGIS Link.
 *
 * @EkanVisualisation(
 *   id = "arcgis",
 *   label = @Translation("ArcGIS Link"),
 *   description = @Translation("ArcGIS preview for ESRI endpoints. Requires a
 *   URL in the resource API field; will not work with resource files."),
 *   formats = {
 *     "rest",
 *     "esri rest",
 *     "arcgis",
 *   }
 * )
 */
class ArcGis extends EkanVisualisationBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'arcgis_base_url' => 'https://www.arcgis.com/apps/mapviewer/index.html',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $help_link = Link::fromTextAndUrl($this->t("Documentation"), Url::fromUri("https://doc.arcgis.com/en/arcgis-online/reference/use-url-parameters.htm"));
    $elements['help'] = [
      '#markup' => $this->t("See ArcGis @help_link. The 'url' parameter will populated with the url to the resource file", ['@help_link' => $help_link->toString()]),
    ];
    $elements['arcgis_base_url'] = [
      '#title' => "ArcGIS viewer base URL",
      '#type' => 'textarea',
      '#default_value' => $this->configuration['arcgis_base_url'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function visualise(EkanResourceEntity $resource): ?array {
    $elements = [];
    $url = NULL;

    try {
      if ($resource->getApiLink()) {
        $url = $resource->getApiLink()->getUrl();
      }
      elseif ($resource->getLocalFileEntity()) {
        $url = Url::fromUri('internal:/' . $resource->getLocalFileEntity()
          ->createFileUrl())->setAbsolute();
      }
      elseif ($resource->getRemoteFileEntity()) {
        $url = Url::fromUri($resource->getRemoteFileEntity()
          ->createFileUrl());
      }
    }
    catch (\Exception $ex) {
      \Drupal::messenger()
        ->addError($this->t("Problem generating arcgis link"));
      Error::logException($this->logger, $ex);
    }

    if ($url instanceof Url) {
      $elements[] = [
        '#type' => 'link',
        '#title' => $this->t('ArcGIS Link'),
        '#url' => Url::fromUri($this->configuration['arcgis_base_url'], [
          'query' => [
            'url' => $url->toString(),
          ],
        ]),
        '#attributes' => ['target' => '_blank'],
      ];
    }

    return $elements;
  }

}

<?php

namespace Drupal\ekan_visuals\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\ekan_visuals\Plugin\Field\VisualisationFieldTrait;

/**
 * Field formatter to Visualisation.
 *
 * @FieldFormatter(
 *  id = "visualisation_formatter",
 *  label = @Translation("Ekan Visualisation"),
 *  field_types = {"string_long"}
 * )
 */
class VisualisationFormatter extends FormatterBase {

  use VisualisationFieldTrait;

  /**
   * Renders a pdf selected plugin for the resource type.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The collection of field items.
   * @param string $langcode
   *   The language code.
   *
   * @return array
   *   A renderable array.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $resource = $this->getResourceFromItems($items);
    $settings = $this->getSettingsFromItems($items);

    $elements = [];

    if (empty($settings) || !$resource) {
      return $elements;
    }

    /** @var \Drupal\ekan_visuals\EkanVisualisationManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.ekan_visualisation');
    foreach ($settings as $plugin_id => $plugin_settings) {
      /** @var \Drupal\ekan_visuals\Plugin\EkanVisualisation\EkanVisualisationInterface $visualisation_plugin */
      try {
        $visualisation_plugin = $plugin_manager->createInstance($plugin_id, $plugin_settings['settings'] ?? []);
      }
      catch (\Exception $ex) {
      }

      if ($visualisation_plugin->isApplicable($resource)) {
        $elements[] = $visualisation_plugin->visualise($resource);
      }
    }

    return $elements;
  }

}

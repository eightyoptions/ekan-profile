<?php

namespace Drupal\ekan_visuals\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\ekan_visuals\Plugin\EkanVisualisation\EkanVisualisationInterface;
use Drupal\ekan_visuals\Plugin\Field\VisualisationFieldTrait;

/**
 * Plugin implementation of the 'Visualisation' widget.
 *
 * @FieldWidget(
 *   id = "visualisation_select",
 *   label = @Translation("Visualisation"),
 *   field_types = {
 *     "string_long"
 *   },
 *   multiple_values = TRUE
 * )
 */
class VisualisationSelector extends StringTextareaWidget {

  use VisualisationFieldTrait;

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items->getValue() && $items->getValue()[0]['value'] ? json_decode($items->getValue()[0]['value'], TRUE) : [];
    $element['title']['#markup'] = '<strong>' . t('Visualisations') . '</strong>';
    /** @var \Drupal\ekan_visuals\EkanVisualisationManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.ekan_visualisation');

    $resource = $this->getResourceFromItems($items);

    $definitions = $plugin_manager->getDefinitions();

    // Sort by definition label.
    uasort($definitions, function ($a, $b) {
      return strcasecmp($a['label'], $b['label']);
    });

    foreach ($definitions as $key => $definition) {
      $visualisation_plugin = $plugin_manager->createInstance($key);
      assert($visualisation_plugin instanceof EkanVisualisationInterface);

      // Hide formats that don't apply (except when editing the resource), where
      // this is done with JS.
      if ($resource instanceof EkanResourceEntity && $form_state->getBuildInfo()['form_id'] != 'resource_form' && $form_state->getBuildInfo()['base_form_id'] != 'resource_form') {
        if (!$visualisation_plugin->isApplicable($resource)) {
          continue;
        }
      }

      /** @var \Drupal\ekan_visuals\Plugin\EkanVisualisation\EkanVisualisationInterface $visualisation_plugin */
      $visualisation_plugin = $plugin_manager->createInstance($key);
      if ($visualisation_plugin->visualisationAccess(\Drupal::currentUser())) {
        // These patterns will match against selected formats
        // using JS regex. If a format matches one of these
        // patterns, this checkbox will be enabled.
        $enable_patterns = [];
        foreach ($definition['formats'] as $format) {
          $enable_patterns[] = "^$format";
        }

        $default_plugin_value = FALSE;
        if (array_key_exists($key, $value)) {
          $default_plugin_value = TRUE;
        }
        $element['visual_type-' . $key]['input'] = [
          '#type' => 'checkbox',
          '#title' => $definition['label'],
          '#default_value' => $default_plugin_value,
          '#plugin_id' => $key,
          '#attributes' => [
            'class' => ['data-vis-checkbox'],
            'data-vis-enable-patterns' => json_encode($enable_patterns),
          ],
        ];
        $settings = $visualisation_plugin->settingsForm($form, $form_state);
        if ($settings) {
          if (array_key_exists($key, $value)) {
            foreach ($value as $plugin_id => $stored_settings) {
              foreach ($stored_settings['settings'] ?? [] as $setting_key => $setting_value) {
                $settings[$setting_key]['#default_value'] = $setting_value;
              }
            }
          }

          $parent_names = array_merge(
            $element['#field_parents'],
            [$items->getName(), 'visual_type-' . $key]
          );
          $checkbox_selector = str_replace('_', '-', implode("-", $parent_names));
          $checkbox_selector = "[data-drupal-selector='edit-$checkbox_selector-input']";

          $element['visual_type-' . $key]['details'] = [
            '#type' => 'details',
            '#title' => $definition['label'],
            '#attributes' => [
              'class' => ['visual-settings-form select-visual-' . $key],
            ],
            '#tree' => TRUE,
            '#open' => TRUE,
            '#states' => [
              'visible' => [
                $checkbox_selector => ['checked' => TRUE],
              ],
            ],
          ];
          $element['visual_type-' . $key]['details']['settings'] = $settings;
        }
      }
    }

    $element['#attached']['library'][] = 'ekan_visuals/show-visualisation-options';

    if ($items->getEntity() instanceof EkanResourceEntity) {
      $element['#attached']['drupalSettings']['ekan_visuals']['resource_format_selector'] = '[data-drupal-selector="edit-resource-format-0-target-id"]';
    }

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getName() == 'field_visualisation' && $field_definition->getTargetEntityTypeId() == 'paragraph') {
      return TRUE;
    }

    if ($field_definition->getName() == 'visualisation' && $field_definition->getTargetEntityTypeId() == 'resource') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $data = [];
    foreach ($values as $key => $value) {
      if (preg_match('/visual_type-(.*)/', $key, $matches)) {
        if ($value['input']) {
          $data[$matches[1]]['settings'] = $value['details']['settings'] ?? [];
        }
      }
    }
    $values['value'] = json_encode($data);
    return parent::massageFormValues($values, $form, $form_state);
  }

}

<?php

namespace Drupal\ekan_visuals\Plugin\Field;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Trait shared by both field formatter and widget.
 */
trait VisualisationFieldTrait {

  /**
   * Given a field item list return the resource from the field.
   */
  private function getResourceFromItems(FieldItemListInterface $items): ?EkanResourceEntity {
    $entity = $items->getEntity();
    if ($entity instanceof ParagraphInterface && $entity->bundle() == 'embedded_resource') {
      return $entity->get('field_resource')->entity;
    }
    elseif ($entity instanceof EkanResourceEntity) {
      return $entity;
    }

    return NULL;
  }

  /**
   * Given a field item list, return the visualisation settings for the field.
   */
  private function getSettingsFromItems(FieldItemListInterface $items): array {

    $entity = $items->getEntity();

    $settings = json_decode($items->getValue()[0]['value'] ?? '{}', TRUE);
    if (empty($settings) && $entity instanceof ParagraphInterface && $entity->bundle() == 'embedded_resource') {
      $resource = self::getResourceFromItems($items);
      if ($resource instanceof EkanResourceEntity) {
        $settings = self::getSettingsFromItems($resource->get('visualisation'));
      }
    }

    return $settings;

  }

}

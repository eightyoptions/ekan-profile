(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.pie_chart = {

    attach: function (context, settings) {
      // Based on prepared DOM, initialize echarts instance.
      $.each(settings.pie_chart, function (key, item) {

        // Skip init if chart is not visible.
        var el = document.getElementById(key);
        if (el.offsetParent === null) {
          return;
        }

        var PieChart = echarts.init(el);

        var option;
        option = {

          tooltip: {
            trigger: 'item'
          },
          legend: {
            orient: 'vertical',
            top: 0,
            left: '66%',
            type: 'scroll'
          },
          series: [
            {
              type: 'pie',
              radius: '80%',
              top: 0,
              right: '33%',
              data: item.data,
              label: {
                position: 'inner'
              },
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        };

        // Use configuration item and data specified to show chart.
        PieChart.setOption(option);
        var chart = PieChart;

        window.addEventListener('resize', function () {
          chart.resize();
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);

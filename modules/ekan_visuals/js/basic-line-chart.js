(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.line_chart = {
    chart: {},
    attach: function (context, settings) {
      // Based on prepared DOM, initialize echarts instance.
      $.each(settings.line_chart, function (key, item) {

        // Skip init if chart is not visible.
        var el = document.getElementById(key);
        if (el.offsetParent === null) {
          return;
        }

        var LineChart = echarts.init(el);
        var xaxis_value = item.x_axis_values;
        var xaxis_label = item.x_axis_label;
        var yaxis_value = item.y_axis_values;
        var yaxis_label = item.y_axis_label;
        var option = {
          color: ['#014587'],
          grid: {
            bottom: 150,
          },
          tooltip: {
            trigger: 'axis',
          },
          xAxis: [
            {
              type: 'category',
              data: xaxis_value,
              name: xaxis_label,
              nameLocation: 'middle',
            }
          ],
          yAxis: [
            {
              type: 'value',
              name: yaxis_label,
              axisLine: {show: true},
              axisLabel: {
                show: true,
              },
            }
          ],
          series: [
            {
              name: 'value',
              type: 'line',
              data: yaxis_value
            }
          ]
        };

        // Use configuration item and data specified to show chart.
        LineChart.setOption(option);
        this.chart = LineChart;
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
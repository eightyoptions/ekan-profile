(function ($, Drupal, once, drupalSettings) {
    Drupal.behaviors.preview_toggle = {
        attach: function (context) {
            once('resource-preview', '.data-and-resource-block .dataset-resource', context).forEach(function (preview_wrapper) {
                var button = $('.preview_btn', preview_wrapper);
                var preview_frame = $('.views-field-visualisation', preview_wrapper);
                preview_frame.hide();
                preview_frame.removeClass('d-none');
                button.on('click', function () {
                    preview_frame.slideToggle('slow', function () {
                        Drupal.attachBehaviors(document, drupalSettings);
                    });
                });
            });
        }
    };

})(jQuery, Drupal, once, drupalSettings);
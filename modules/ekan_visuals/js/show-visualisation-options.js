(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.show_visualisation_options = {
    attach: function (context, settings) {
      if (typeof drupalSettings.ekan_visuals !== 'undefined') {
        var autocomplete = document.querySelector(drupalSettings.ekan_visuals.resource_format_selector);
        autocomplete.addEventListener('ready', this.showVisuals);
        autocomplete.addEventListener('focus', this.showVisuals);
        autocomplete.addEventListener('change', this.showVisuals);
        autocomplete.addEventListener('keyup', this.showVisuals);

        // Run on load;
        this.showVisuals({ target: autocomplete });
      }
    },
    showVisuals: function (event) {
      var autocomplete = event.target;
      var selectedFormat = $(autocomplete).val().toString().trim();
      $('.data-vis-checkbox').each((k, element) => {
        $(element).attr('disabled', 'true');
        let patterns = $(element).data('vis-enable-patterns');

        if (patterns.length == 0) {
          $(element).removeAttr('disabled');
        }

        patterns.forEach((pattern) => {

          // Case-insensitive matching.
          var regex = new RegExp(pattern, 'i');
          if (selectedFormat.match(regex)) {
            $(element).removeAttr('disabled');
          }
        });
      });
    },
  };

})(jQuery, Drupal, drupalSettings);

(function ($, Drupal, once) {
  Drupal.behaviors.ekan_detect_pdf = {
    attach: function (context, settings) {
      const elements = once('ekan_detect_pdf', 'input[type="file"]', context);
      elements.forEach(function(el) {
        $(el).on('change.autoFileUpload', function(event) {
          mimeType = $(event.target.files)[0].type;
          if (mimeType == 'application/pdf') {
            // For uploaded pdf file, tick PDF option by default.
            $('#vis-input-pdf_viewer').prop('checked', true);
            // Also remove style "display=none" added with form #states.
            $('#edit-visualisation-visual-type-pdf-viewer-details').removeAttr('style');
          }
        });
      });
    }
  };
})(jQuery, Drupal, once);
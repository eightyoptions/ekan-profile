# Installing EKAN

## The Short Version

```
  composer create-project eightyoptions/ekan_distribution:2.0.0 drupal
```

## The Long Version

```
  composer create-project drupal/recommended-project:^10 drupal
  cd drupal
  composer config extra.enable-patching --json true
  composer config --no-plugins allow-plugins.cweagans/composer-patches true
  composer config minimum-stability dev
  composer require eightyoptions/ekan:^2.0

```


## Setting up Nginx for access to Project Open Data files.

Ensure that nginx can serve up the catalog.xml file e.g.

Example nginx.conf
```
#Allow requests to /catalog.xml to hit our controller.
location = /catalog.xml {
    access_log off;
    log_not_found off;
    try_files $uri @drupal;
}
```